# Resources

[TOC]

## General

www.google.com

https://ss64.com/ - Description of all common Linux/MacOS/Windows commands

https://www.asciitohex.com/ - Convert different base numbers

https://gchq.github.io/CyberChef/ - Bases, Encryption, Forensics, Pretty text

`strings` - Show UTF-8 strings

`file` - Show filetype 

`nano` - cli text editor (easiest and maybe fastest)

`vim ` - cli text editor 2 (most common)

`emacs` - cli text editor 3 (best)

Typora - Creating and reading markdown files like this

### [Bash](#Bash)

### Training

https://overthewire.org/wargames/bandit/ - Linux commands

https://overthewire.org/wargames/leviathan/ - Advanced Linux/Problem solving

https://2018game.picoctf.com - General cyber skills

https://vim-adventures.com/ - vim training

https://learngitbranching.js.org/ - Git commands

## Languages

https://exercism.io/ - Learn languages with different problems

https://www.codewars.com/ - Good for perfecting knowledge of languages

https://www.edx.org/ - Actual college classes

https://www.jetbrains.com/ - IDEs for many languages

https://hyperskill.org/ - Coding projects

https://leetcode.com/ - Good for job interviews

### Bash

https://www.learnshell.org/

### Python

https://www.learnpython.org/ - Python 3

### C

https://www.freecodecamp.org/news/the-c-beginners-handbook/

https://overthewire.org/wargames/manpage/ - Breaking C misconceptions

https://gitlab.com/QRK1TT13/learncyber - And network/hacking

### SQL

https://www.codecademy.com/learn/learn-sql

https://www.w3schools.com/sql/

### JavaScript 

https://www.codecademy.com/learn/introduction-to-javascript

https://open.appacademy.io/ - Full stack

### Assembly

https://www.cs.virginia.edu/~evans/cs216/guides/x86.html - There's this thing called the stack

### PHP

https://www.codecademy.com/learn/learn-php

### Ruby

https://www.codecademy.com/learn/learn-ruby

## Cryptography

https://www.dcode.fr/ - Encryption tools

`hashcat` - Password cracking

`john` - Password cracking

### [Python](#Python)

### More info

https://www.tutorialspoint.com/cryptography_with_python/index.htm

[Chinese Remainder Theorem](https://en.wikipedia.org/wiki/Chinese_remainder_theorem#:~:text=In number theory%2C the Chinese,the divisors are pairwise coprime.)

### Training

https://cryptohack.org/ - Basics to expert

https://overthewire.org/wargames/krypton/ - Tests knowledge and identification skills

https://www.hackthebox.eu/ - Assorted types of practical Crypto

https://www.root-me.org/ - Assorted types of practical Crypto

## Forensics

`grep` - find in file

### Networks

#### Packet Inspection

Wireshark - Application for packet capturing and network inspection

`tshark` - cli Wireshark scripting

Network Miner - Application for packet inspection

PacketTotal - PCAP analysis gui

`ngrep` - packet grep

`snort` - Packet inspection

PCAPfix - PCAP repairing

#### Active communications

Burpsuite - Connection packet inspection

### File systems

Autopsy - File system analysis

FTK - File system analysis

`testdisk` - Recovering partition tables, deleted files, and fixing corruption

https://selvamvasu.wordpress.com/2014/08/01/inode-vs-ext4/ - Linux filesystems

### Images

Stegsolve - Image steganography

`binwalk` - Filesystem/image analysis

`exfiltool` - Image analysis

Pillow (Python Image library) - Scripting library for images

HxD - Hex editor

`hexdump` - Hex dump

Gimp - Image manipulation

### Volatile memory

#### Volatility

`volatility`

`ethscan` - Extracting packets from memory dumps (plugins for finding SQL and internet history)

### Audio

Sonic Visualizer - Manipulate audio files

Audacity - Audio file viewing tool

### PDFs

Adobe Acrobat - PDF viewer and manipulator

`qpdf` - exploring pdf information

### More Info

https://trailofbits.github.io/ctf/forensics/ - Forensics concepts

### Training

https://2018game.picoctf.com/ - Good intro to basic/intermediate

https://www.hackthebox.eu/ - Mostly offence but does have labs for all skills

https://www.hackthissite.org/ - Steg/Linux problems

https://www.root-me.org/

## Web

Burpsuite - Connection packet inspection application

### [SQL](#SQL)

### [JavaScript](#JavaScript)

### [PHP](#PHP)

### Training

https://www.hackthebox.eu/ - Mostly offence but does have labs for all skills

https://www.hackthissite.org/ - "Realistic"/Javascript

https://www.root-me.org/

## Reversing

### Ghidra

https://medium.com/@nowayout/practice-with-ghidra-c23fba84db66

### OllyDbg

https://www.hackers-arise.com/post/2017/10/03/reverse-engineering-malware-part-5-ollydbg-basics

### Training

https://2018game.picoctf.com/ - Good intro to basic/intermediate

https://microcorruption.com/login - Live reverse engineering

https://www.hackthebox.eu/

https://www.root-me.org/

## PWN

### [Reversing](#Reversing)

### [C](#C)

### [Assembly](#Assembly)

### Pwn Tools

https://github.com/Gallopsled/pwntools

### Training

https://www.root-me.org/

https://overthewire.org/wargames/narnia/

https://overthewire.org/wargames/behemoth/

https://www.hackthebox.eu/

https://overthewire.org/wargames/utumno/

https://overthewire.org/wargames/maze/

https://overthewire.org/wargames/vortex/

https://overthewire.org/wargames/semtex/ - Special

https://overthewire.org/wargames/manpage/

## Offense

[SANS 560](http://libgen.li/search.php?req=sans+560&open=0&res=25&view=simple&phrase=1&column=def)

### [Ruby](#Ruby) - Not really needed

### Training

https://tryhackme.com/ - Good for basics. Don't pay

https://www.hackthebox.eu/

https://www.vulnhub.com/

## Defense

### [Offense](#Offense)

## Other Topics

### AI

https://course.fast.ai/

https://www.freecodecamp.org/learn/machine-learning-with-python/tensorflow/introduction-to-tensorflow

https://ocw.mit.edu/resources/res-18-005-highlights-of-calculus-spring-2010/highlights_of_calculus/big-picture-of-calculus/

https://www.coursera.org/learn/deep-neural-network?specialization=deep-learning

https://www.edx.org/

https://cs231n.github.io/

http://www.deeplearningbook.org/

https://developers.google.com/machine-learning/crash-course

### Big Data

https://www.edx.org/ - College classes

## Other recourses

https://doc.lagout.org/ - Any computer book ever

http://libgen.li/

http://pdf.textfiles.com/security/googlehackers.pdf - Google dorking

https://ctftime.org/ - CTF's and writeups

https://www.root-me.org/

https://www.freecodecamp.org/

