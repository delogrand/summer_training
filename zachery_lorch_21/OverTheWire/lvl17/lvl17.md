# overthewire writeup
Zachery Lorch

### Level6->level7
1. connect to server using ssh protcol

```bash
ssh bandit16@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. super easy diff command 

```bash
diff -c password.old password.new
```

4. select the line that has changed

flag is: kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd
