###### Shabri Pelia

### Level 4 --> Level 5

1. ssh bandit4@bandit.labs.overthewire.org -p 2220
2. ls and cd inhere
3. file ./* helps you get information on every file in the inhere directory
   - You can also not change directory and just do inhere/*
4. File 7 is the only file that has ASCII Text
5. cat file_7
   - Gives us the password: koReBOKuIDDepwhWk7jZC0RTdopnAYKh



TIPS/TAKE AWAYS:

* file command = gives information on the files
* use file ./* to give information on every file in the directory



