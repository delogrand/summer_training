# overthewire writeup
Zachery Lorch

### Level9->level10
1. connect to server using ssh protcol

```bash
ssh bandit10@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. strings to only look at acsii and grep it to look at beginng
= signs 

```bash
strings data.txt | grep ^=
```

flag is: VkdobElIQmhjM04zYjNKa0lHbHpJRWxHZFd0M1MwZHpSbGM0VFU5eE0wbFNSbkZ5ZUVVeGFIaFVUa1ZpVlZCU0NnPT0K

