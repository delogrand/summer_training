### grep_1

1. Find the flag in the file located at /problems/grep-1_4_0431431e36a950543a85426d0299343e using grep
2. 
```bash
grep CTF < /problems/grep-1_4_0431431e36a950543a85426d0299343e/file
picoCTF{grep_and_you_will_find_d66382d8}
```

Flag is picoCTF{grep_and_you_will_find_d66382d8}