# PicoCTF General Warmup 1 
 ## Problem: 
Need to conver 0x41 to ASCII text.
## Solution 
Using an online converter reveals 0x41 in hex is the letter "A” in ASCII text.
##  Result
The flag is `picoCTF{A}`

