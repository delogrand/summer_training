### OverTheWire Bandit 2.3 Writeup

1. Connect to the server using SSH on port 2220

```bash 
$ ssh -p 2220 bandit2@bandit.labs.overthewire.org
```

2. Read what files/directories there are 

```bash
$ ls
```

3. Read the file name to get password

```bash
$ cat 'spaces in this filename'
```

Flag: UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK