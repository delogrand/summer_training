### Level 13 --> Level 14

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit13@bandit.labs.overthewire.org
```

2. Using the private key login to bandit14
```bash
$ ssh -i sshkey.private bandit14@localhost
```

3. Told from goal that the password is stored at /etc/bandit_pass/bandit14
```bash
$ cat /etc/bandit_pass/bandit14
4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e
```

Flag is 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e