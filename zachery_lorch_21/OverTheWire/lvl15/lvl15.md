# overthewire writeup
Zachery Lorch

### Level5->level6
1. connect to server using ssh protcol

```bash
ssh bandit15@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. use ssl to open the connect to the port

```bash
openssl s_client -connect 127.0.0.1:30001
```
4. sumbmit the old key by just typing it and `enter` 


flag is: cluFn7wTiGryunymYOu4RcffSxQluehd
