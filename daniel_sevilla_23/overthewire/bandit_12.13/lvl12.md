### Bandit Write Up
### Level 12 --> 13
ssh bandit12@bandit.labs.overthewire.org -p 2220
mkdir /tmp/temp
xxd -r data.txt > 1.txt
file *
gzip -cd 1.txt > 2.txt
file *
bzip2 -cd 2.txt > 3.txt
file *
gzip -cd 3.txt > 4.txt
file *
tar -xf 4.txt
file *
tar -xf data5.bin
file *
bzip2 -cd data6.bin > 7.txt
file *
tar -xf 7.txt
file *
gzip -cd data8.bin
# Flag is 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL