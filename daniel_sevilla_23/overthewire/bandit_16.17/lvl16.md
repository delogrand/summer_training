### Bandit Write Up
### Level 16 --> 17
ssh bandit16@bandit.labs.overthewire.org -p 2220
nmap localhost -p 31000-32000
openssl s_client -connect localhost:31790 #after trying other ports
openssl s_client -connect localhost:31790 > /tmp/ssh17.private
chmod 600 /tmp/ssh17.private
nano /tmp/ssh17.private #delete everything but the RSA key
ssh -i /tmp/ssh17.private bandit17@localhost
cat /etc/bandit_pass/bandit17
# Flag is xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn