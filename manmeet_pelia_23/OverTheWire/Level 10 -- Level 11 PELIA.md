###### Shabri Pelia

### Level 10 --> Level 11

1. ssh bandit10@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed
3. file data.txt and ASCII text
4. cat data.txt and a long string of unreadable stuff is available - told that it is base64 encoded data
5. base64 command: can encode and decode something in base64
6. base64 -d data.txt
   - File says "The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR"



TIPS/TAKE AWAYS:

* **base64** command: -d decode 



