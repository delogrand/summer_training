### HEEEEEEERE'S Johnny! 

1. So we have to brute force the password for the account using the passwd and shadow files given.
2. Using the vm we installed we can run the password cracking software called John the ripper. You will need to grant root privliage before being able to execute it.
3.```bash
$ sudo john shadow
root:password1:17770:0:99999:7:::

1 password hash cracked, 0 left
```
4. Now with the proper username and password connect to the server using netcat.
```bash
c 2018shell.picoctf.com 40157
Username: root
Password: password1
picoCTF{J0hn_1$_R1pp3d_1b25af80}
```

Flag is picoCTF{J0hn_1$_R1pp3d_1b25af80}