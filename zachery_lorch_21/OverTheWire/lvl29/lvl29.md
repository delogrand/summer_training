# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. go to my tmp file so I can edit things

```bash
cd /tmp/dummy
```

4. git clone with different name than repo and provide password

```bash
git clone ssh://bandit27-git@localhost/home/bandit27-git/repo bandit29
```
password is old key

5. the key here is not to just lok at branches but --all branches

```bash
git show-branch --all
```
here we see way more branches than before. 

6. noticing from the readme of master is said the password was not in production which means 
it could be in development or dev branch
```bash 
git checkout dev
```
7. open readme and get flag

```bash 
cat README.md
```

flag: 5b90576bedb2cc04c86a9e924ce42faf