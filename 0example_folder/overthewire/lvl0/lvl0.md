# OverTheWire Bandit Writeup

Addison Whitney

### Level 0 --> Level 1

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit0@bandit.labs.overthewire.org
```

2. Enter "**bandit0**"as password

3. Read the files in the directory

```bash
bandit0@bandit:~$ ls
readme
```

4. Read the readme file

```bash
bandit0@bandit:~$ cat readme
boJ9jbbUNNfktd78OOpsqOltutMc3MY1
```

Flag is: boJ9jbbUNNfktd78OOpsqOltutMc3MY1
