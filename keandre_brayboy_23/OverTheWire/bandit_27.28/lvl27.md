### Level 27 --> Level 28

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit27@bandit.labs.overthewire.org
```

2. Clone the git repository

```bash
$ git clone ssh://bandit27-git@localhost/home/bandit27-git/repo
```

3. Check what's inside

```bash
$ ls repo
README
```

4. Now just read the file for the password.

```bash
$ cat README
The password to the next level is: 0ef186ac70e04ea33b4c1853d2526fa2
```

Flag is 0ef186ac70e04ea33b4c1853d2526fa2