# Bandit Level 4 -> 5 Write Up
 ## Problem: 
Find password for bandit5
## Solution 
1. See what files are in the current directory with `ls`, there is a directory called inhere
1. switch to that directory using `cd inhere` 
1. Using `ls` shows the names of 10 files
1. Use the `file ` command which given a name of a file prints the file name and type of file. Using `file ./*` will print all files in the directory that have `./` at the beginning and the `*` means any other characters can fill the rest of the file name
1. Observe that the only human readable type shown is ”./-file07 ASCII text” while the others read ”data”. Use `cat ./-file07` to read the file

##  Result
The password for bandit5 is in the file and is `koReBOKuIDDepwhWk7jZC0RTdopnAYKh`

