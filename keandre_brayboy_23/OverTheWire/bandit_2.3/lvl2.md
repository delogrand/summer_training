### Level 2 --> Level 3 

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit2@bandit.labs.overthewire.org
```

2. Search for files
```bash
$ ls
spaces in this file name
```

3. Read the file by including quotation marks around the name.
```bash
$ cat "spaces in this file name"
UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
```

Flag is UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK

