### Level 3 --> Level 4 

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit3@bandit.labs.overthewire.org
```

2. Search for files within a folder
```bash
$ find inhere
inhere
inhere/.hidden
```

3. Read the hidden file using cat
```bash
$ cat inhere/.hidden
pIwrPrtPN36QITSp3EQaw936yaFoFgAB
```

Flag is pIwrPrtPN36QITSp3EQaw936yaFoFgAB

