# overthewire writeup
Zachery Lorch

### Level4->level5
1. connect to server using ssh protcol

```bash
ssh bandit14@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. send the old password to the port 30000 on localhost

```bash
echo {old key} | nc 127.0.0.1 30000
```
breakdown
echo makes the port repeat the given info
piping it to netcat on localhost ip {127.0.0.1} 
over the port 30000

flag is: BfMYroe26WYalil77FoDi9qh59eK5xNr

