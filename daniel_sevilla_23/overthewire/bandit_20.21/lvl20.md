### Bandit Write Up
### Level 20 --> 21
ssh bandit20@bandit.labs.overthewire.org -p 2220
nc -lvp 21212 #starts a netcat server listening on port 21212
### Open new terminal
ssh bandit20@bandit.labs.overthewire.org -p 2220
./suconnect 21212
### The two terminals have "connected" to each other
### On terminal with nc server, send the lvl20 password by pasting the password and hitting enter
### Verbose allows us to see what the ./suconnect terminal sends to the nc server
# Flag is gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr