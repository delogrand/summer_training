###### Shabri Pelia

### Level 2 --> Level 3

1. ssh bandit2@bandit.labs.overthewire.org -p 2220
2. ls and found file called "spaces in this filename"
3. cat 'spaces in this filename'
   - Gives us the password: UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK



TIPS/TAKE AWAYS:

* If file name has spaces in them, use " " or ' '



