# PicoCTF Reversing Warmup 1  
 ## Problem: 
Need to run a file in a specific directory in the picoCTF shell.
## Solution 
Use `cd  /problems/reversing-warmup-1_2_a237211c4be8902c67102f827027e633` to navigate to the correct folder and then `./run` to run the program `run` and this reveals the flag. 
##  Result
The flag is `picoCTf{welc0m3_t0_r3VeRs1nG}`

