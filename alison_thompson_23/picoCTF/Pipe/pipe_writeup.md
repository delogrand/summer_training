# PicoCTF pipe 
 ## Problem 
Need to find the flag by connecting to a given host on a specific port using netcat, but connecting prints text and makes it almost impossible to find the flag by hand.
## Solution 
Using the command 
```
alit633@pico-2018-shell:~$ nc 2018shell.picoctf.com 44310 | grep ”pico” ```
The part of this command before the pipe connects to the given host on a tcp connection using the port 44310. Piping it into `grep ”pico”`then sifts through the output from the connection and leaves only the line with the flag picoCTF{almost_like_mario_a13e5b27}.`
##  Result
The flag is ` picoCTF{almost_like_mario_a13e5b27}`

