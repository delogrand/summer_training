### Reversing_Warmup_1

1. Open up the picoCTF shell and go to the given location
```bash
cd  /problems/reversing-warmup-1_4_6b2499250c4624337a1948ac374c4934
```

2. Execute the program.

```bash
./run
picoCTF{welc0m3_t0_r3VeRs1nG}
```
Flag is picoCTF{welc0m3_t0_r3VeRs1nG}

