# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 6 --> Level 7

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit6@bandit.labs.overthewire.org
```

2. Enter flag from bandit 5.6 **DXjZPULLxYr17uwoI01bNLQbtFemEgo7** as 
password

3. Since the notes from the bandit site tell us the file is on the server, we simpliy plug the conditions into a find command.
```bash
bandit6@bandit:~$ find / -size 33c -user bandit7 -group bandit6 2>/dev/null
/var/lib/dpkg/info/bandit7.password
```

4. We continue to cd to the file dir and read the file
```bash
bandit6@bandit:/$ cd /var/lib/dpkg/info
bandit6@bandit:/var/lib/dpkg/info$ cat bandit7.password
HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs
```

The flag is: HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs

Helpful page:
http://man7.org/linux/man-pages/man1/find.1.html
https://superuser.com/questions/179978/avoid-permission-denied-spam-when-using-find-command