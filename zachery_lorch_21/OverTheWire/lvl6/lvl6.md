# overthewire writeup
Zachery Lorch

### Level6->level7
1. connect to server using ssh protcol

```bash
ssh bandit6@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. use find command with critera give and push permission denied errors 
```bash
find / -size 33c -user bandit7 -group bandit6 -print 2>/dev/null
```
break down: find / look in everything on server
size is 33 bytes and user and group are defined
print is the defualt action of find yet it helps 
	with pushing the errors to the dev null folder
2>/dev/null pretty much just redicts errors to this 
folder so I dont have to see them

3. cat file show
```bash 
cat /var/lib/dpkg/info/bandit7.password
```

flag is: HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs