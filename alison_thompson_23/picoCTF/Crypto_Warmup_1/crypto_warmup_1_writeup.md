# PicoCTF Crypto Warmup 1 
 ## Problem: 
Need to decode `llkjmlmpadkkc` using a table of shifted alphabets and a given key `thisisalilkey`.
## Solution 
Each letter in `thisisalittlekey` represents a different Cesar cipher that corresponds to the letter of the encrypted message in the same position. Ex. The `t` corresponds to the first letter of the encrypted message `l`. This tells us that `l` is shifted according to the cipher a -> t. Using the table this gives the first letter of the decoded message to be `S`. Repeating this process for the entire message gives the answer `SECRETMESSAGE`. 
##  Result
The flag is `picoCTF{SECRETMESSAGE}`

