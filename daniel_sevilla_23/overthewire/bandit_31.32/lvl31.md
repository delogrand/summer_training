### Bandit Write Up
### Level 31 --> 32
ssh bandit31@bandit.labs.overthewire.org -p 2220
mkdir /tmp/gitb31
cd /tmp/gitb31
git clone bandit31-git@localhost:/home/bandit31-git/repo
cd repo
ls -la #Has .gitignore file
cat README.md #Need to create a file called key.txt with text
cat .gitignore #Wont allow my text file to be pushed
rm .gitignore
echo May I come in? > key.txt
git add .
git commit -m "Added key.txt"
git push
# Flag is 56a9bf19c63d650ce78e6ec0354ee45e