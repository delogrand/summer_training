# PicoCTF Logon

## Problem 
Need to sign in as admin on the site `http://2018shell.picoctf.com:57252` to get the flag
## Solution 
The site will let you log in with any combination besides with admin as the username. After clicking sign in it shows a page that says you are signed in but the flag is hidden from view. Under inspect element there is a cookies tab with a row titled "admin” and with the value "False”. Changing this to "True" and refreshing reveals the flag to be ` http://2018shell.picoctf.com:57252/flag`.

                                                                                                                           
                                                                                                                         ##  Result
The flag is ` picoCTF{l0g1ns_ar3nt_r34l_2a968c11}`
