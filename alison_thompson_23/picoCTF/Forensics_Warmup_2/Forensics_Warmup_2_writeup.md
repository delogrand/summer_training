# PicoCTF Forensics Warmup 2 
 ## Problem: 
Find the flag given a PNG file that does not open.
## Solution 
I right clicked on the PNG file and clicked ‘’Edit’’ and it opened the file in paint revealing the flag. 
##  Result
The flag is `picoCTF{extensions_are_a_lie}`

