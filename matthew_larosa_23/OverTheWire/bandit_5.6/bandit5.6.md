# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 5 --> Level 6

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit5@bandit.labs.overthewire.org
```

2. Enter flag from bandit 4.5 **koReBOKuIDDepwhWk7jZC0RTdopnAYKh** as 
password

3. Read the files in the directory

```bash
bandit5@bandit:~$ ls
inhere
```

4. I noticed "inhere" is a directory so I cd in
```bash
bandit5@bandit:~$ cd inhere
```

5. Multiple Files appear so we determine which ones meet the requirments listed on the site with the find command.
```bash
bandit5@bandit:~/inhere$ ls
maybehere00  maybehere03  maybehere06  maybehere09  maybehere12  maybehere15  maybehere18
maybehere01  maybehere04  maybehere07  maybehere10  maybehere13  maybehere16  maybehere19
maybehere02  maybehere05  maybehere08  maybehere11  maybehere14  maybehere17
```

6. Since we are attempting to find a file wih a size of 1033 bytes, is readable, but not executable we use the following command
```bash
bandit5@bandit:~/inhere$ find -size 1033c -readable \! -executable
./maybehere07/.file2
```
7. With the information received we cd into the directory and read the file

```bash
bandit5@bandit:~$ cd inhere/maybehere07
bandit5@bandit:~/inhere/maybehere07$ cat ".file2"
DXjZPULLxYr17uwoI01bNLQbtFemEgo7
```

The flag is: DXjZPULLxYr17uwoI01bNLQbtFemEgo7

Helpful page:
http://man7.org/linux/man-pages/man1/find.1.html