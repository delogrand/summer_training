# PicoCTF Client Side is Still Bad 
 ## Problem 
Find the flag given the website with a login screen. 
## Solution 
Right clicking anywhere and clicking `Inspect` pulls up a menu that shows the code for the page. 

```
  function verify() {
    checkpass = document.getElementById(”pass”).value;
    split = 4;
    if (checkpass.substring(split*7, split*8) == ‘‘}’‘) {
      if (checkpass.substring(split*6, split*7) == ‘‘d366’‘) {
        if (checkpass.substring(split*5, split*6) == ‘‘d_3b’‘) {
         if (checkpass.substring(split*4, split*5) == ‘‘s_ba’‘) {
          if (checkpass.substring(split*3, split*4) == ‘‘nt_i’‘) {
            if (checkpass.substring(split*2, split*3) == ‘‘clie’‘) {
              if (checkpass.substring(split, split*2) == ‘‘CTF{‘‘) {
                if (checkpass.substring(0,split) == ‘‘pico’‘) {
                  alert(“You got the flag!“)
```
Putting the last sections of the split together reveals the flag, and when inputted as the password returns the message `You got the flag!`. 
##  Result
The flag is ` picoCTF{client_is_bad_3bd366}}`

