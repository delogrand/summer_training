# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 4 --> Level 5

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit4@bandit.labs.overthewire.org
```

2. Enter flag from bandit 3.4 **pIwrPrtPN36QITSp3EQaw936yaFoFgAB** as 
password

3. Read the files in the directory

```bash
bandit2@bandit:~$ ls
inhere
```

4. I noticed "inhere" is a directory so I cd in
```bash
bandit4@bandit:~$ cd inhere
bandit4@bandit:~/inhere$ ls
-file00  -file01  -file02  -file03  -file04  -file05  -file06  -file07  -file08  -file09
```

5. Multiple Files appear so we determine which ones have data with the file command
```bash
bandit4@bandit:~/inhere$ file ./*
./-file00: data
./-file01: data
./-file02: data
./-file03: data
./-file04: data
./-file05: data
./-file06: data
./-file07: ASCII text
./-file08: data
./-file09: data
```

6. Read the file that is in ASCII with the cat command
```bash
bandit4@bandit:~/inhere$ cat ./-file07
koReBOKuIDDepwhWk7jZC0RTdopnAYKh
```

The flag is: koReBOKuIDDepwhWk7jZC0RTdopnAYKh