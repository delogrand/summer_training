# overthewire writeup
Zachery Lorch

### Level4->level5
1. connect to server using ssh protcol

```bash
ssh bandit4@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. cd into inhere
```bash
cd inhere
```
4. use find command to and pipe it with to show the data and then
pipe it to grep it for those that have text

```bash
find - type f | xargs | grep text
```

breaking this down ```-type f | xarge``` will show all the files
data type (so data or ASCII or UNICODE) and then ```| grep text```
will only show the ones that have text in them.

5. cat -file07 with reletive path because it starts with '-'

```bash 
cat ./-file07
```

flag is: koReBOKuIDDepwhWk7jZC0RTdopnAYKh