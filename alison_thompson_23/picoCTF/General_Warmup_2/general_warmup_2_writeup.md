# PicoCTF General Warmup 2 
 ## Problem: 
Need to convert the number 27 (base 10) to binary (base 2).
## Solution 
The highest base 2 that goes into 27 is 16, then 8, then 2, then 1. All together they add to 27 so assigning ones to the appropriate locations give 
##  Result
The flag is `picoCTF{11011}`

