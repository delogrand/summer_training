## Problem: 
Find password for bandit3

## Solution
1. See what files are in the current directory with `ls`
1. There is a file called 'spaces in this filename' but you can't use `cat  spaces in this file name` because it is invalid syntax 
1. Use the command `cat "spaces in this filename"` to view the contents (use quotes to avoid syntax error)

# Result
The password for bandit3 is in the file and is `UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK`
