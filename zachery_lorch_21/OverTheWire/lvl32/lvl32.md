# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. relize you can open a shell in a shell with $0

```bash
$0
```
4. then i wanted to have it easier so I ran normal bash

```bash
/bin/bash
```

5. and then I looked at the next levels password

```bash
cat /etc/bandit_pass/bandit33
```

flag: c9c3199ddf4121b10cf581a98d51caee