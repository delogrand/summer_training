﻿# Bandit Level 1 -> 2 Write Up
 ## Problem: 
Find password for bandit2
## Solution 
1. See what files are in the current directory with `ls`
1. There is a file called – but you can’t use `cat -` because it is invalid syntax
1. Use the command `cat ./-` to view the contents 

##  Result
The password for bandit2 is in the file and is `CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9`

