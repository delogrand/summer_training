###### Shabri Pelia

### Level 5 --> Level 6

1. ssh bandit5@bandit.labs.overthewire.org -p 2220
2. ls and cd inhere
3. ls inhere (lots of directories with lots of files inside appear)
4. Used find command with specifications given
   - find -size 1033c 
     - c = bytes
     - w = two-byte words
     - k = kilobytes
     - M = Megabytes
     - G = Gigabytes
     - Can also use +/- for bigger or smaller than (ex: -512k is smaller than 512 kilobytes)
   - find -executable (I think ! in front of -executable negates it <u>**fact check this**</u>)
5. find -size 1033c gives only one file
6. cat ./maybehere07/.file2
   - Gives us the password: DXjZPULLxYr17uwoI01bNLQbtFemEgo7



TIPS/TAKE AWAYS:

* find command = can find things in the specific parameters
* Parameters
  * -size
  * -executable
  * -name
  * -user
  * -group
  * -iname (ignore cases)
  * -type (f = file or d = directory)



