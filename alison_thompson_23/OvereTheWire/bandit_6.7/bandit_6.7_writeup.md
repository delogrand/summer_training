# Bandit Level 6 -> 7 Write Up
 ## Problem: 
Find password for bandit7
## Solution 
1. Use `cd ..` until cannot back out any further (twice)
1.  I used `find –user bandit6 –group bandit7` and the only file that does not say "Permission denied” is ` ./var/lib/dpkg/info/bandit7.password`
1. Use `cat ./var/lib/dpkg/info/bandit7.password` to read the file

##  Result
The password for bandit7 is in the file and is ` HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs`
