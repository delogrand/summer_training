### Bandit Write Up
### Level 29 --> 30
ssh bandit29@bandit.labs.overthewire.org -p 2220
mkdir /tmp/gitb29
cd /tmp/gitb29
git clone bandit29-git@localhost:/home/bandit29-git/repo
cd repo
ls -la
cat README.md #Hint is in the "no password in production"
git log #Only two commits; Assume other one doesnt have it
ls -la /.git/*/* #Gitpacks are new this time (or didnt notice). Did a bit of research to find out they hold a lot of info
cd .git/objects/pack
git verify-pack -v pack-968...4c2.pack #There are 5 commits here. 2 of which were pushed. Check last one because of "in production"
cd ../../.. #Back to repo
git reset --hard bc83328
cat README.md
# Flag is 5b90576bedb2cc04c86a9e924ce42faf