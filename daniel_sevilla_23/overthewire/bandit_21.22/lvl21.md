### Bandit Write Up
### Level X --> X
ssh banditX@bandit.labs.overthewire.org -p 2220
cd /etc/cron.d/
ls -ls
cat cronjob_bandit22 #Shows a shell script running for bandit22 in /usr/bin
cd /usr/bin
cat cronjob_bandit22.shell #Shows a file with a random name in /temp as having the password for bandit22
cat /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
# Flag is Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI