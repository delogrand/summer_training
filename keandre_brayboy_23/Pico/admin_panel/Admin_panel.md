### Admin Panel

1. Download the file and open it in wireshark.
2. Follow the TCP stream for a login into the admin account.
3. On stream 5 the admin login appears.

Flag is picoCTF{n0ts3cur3_9feedfbc}
