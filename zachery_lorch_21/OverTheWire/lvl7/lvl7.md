# overthewire writeup
Zachery Lorch

### Level7->level8
1. connect to server using ssh protcol

```bash
ssh bandit7@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. use pipe and grep to look near *millionth*

```bash
cat data.txt | grep millionth
```

flag is: cvX2JJa4CFALtqS87jk27qwqGhBM9plV