###### Shabri Pelia

### Level 1 --> Level 2

1. ssh bandit1@bandit.labs.overthewire.org -p 2220
2. ls and found file called "-"
3. There are three ways to call - since it is a special character (considered standard input) and cat - won't work
   1. < -
   2. ./-
   3. ~/-

4. cat < - gives us the password: CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9



TIPS:

* Shift + Ctrl + C to copy something on Git Bash



