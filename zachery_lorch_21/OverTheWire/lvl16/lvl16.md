# overthewire writeup
Zachery Lorch

### Level6->level7
1. connect to server using ssh protcol

```bash
ssh bandit16@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. scan all ports in range given

```bash
netcat -v -z -n -w 1 127.0.0.1 31000-32000
```

4. only one port shows up as open and vaild ssl into it

```bash
openssl s_client -connect 127.0.0.1:31790
```
4. sumbmit the old key by just typing it and `enter` 

5. copy and past the RSA private key into a file on local machine
connect to next proglem using this private key

```bash
ssh -i sshkey.private1 bandit17@bandit.labs.overthewire.org -p 2220
```
6. once on sever read password file as done previsouly to get
flag

```bash
cat /etc/bandit_pass/bandit17
```

flag is: xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn
