### Level 29 --> Level 30

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit29@bandit.labs.overthewire.org
```

2. Clone the git repository

```bash
$ git clone ssh://bandit29-git@localhost/home/bandit29-git/repo
```

3. Same as before lets check that readme file

```bash
$ cat /repo/README.md
# Bandit Notes
Some notes for bandit30 of bandit.

## credentials

- username: bandit30
- password: <no passwords in production!>
```

4. Time to check the logs.

```bash
$ git log
commit 208f463b5b3992906eabf23c562eda3277fea912
Author: Ben Dover <noone@overthewire.org>
Date:   Thu May 7 20:14:51 2020 +0200

    fix username

commit 18a6fd6d5ef7f0874bbdda2fa0d77b3b81fd63f7
Author: Ben Dover <noone@overthewire.org>
Date:   Thu May 7 20:14:51 2020 +0200

    initial commit of README.md
```

5. Well then since that gave nothing time to see if there are other branches

```bash 
$ git branch -r
 origin/HEAD -> origin/master
 origin/dev
 origin/master
 origin/sploits-dev
```

6.  Lets see if the dev branch has anything.

```bash 
$ git checkout dev
$ ls
code  README.md
```

7. Lets check the readme

```bash 
$ cat README.md
# Bandit Notes
Some notes for bandit30 of bandit.

## credentials

- username: bandit30
- password: 5b90576bedb2cc04c86a9e924ce42faf
```

Flag is 5b90576bedb2cc04c86a9e924ce42faf