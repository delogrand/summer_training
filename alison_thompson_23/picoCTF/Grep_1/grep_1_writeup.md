# PicoCTF grep 1
 ## Problem 
Need to find the flag given a file that is extremely difficult to look through by hand. 
## Solution 
Using the command 
```
alit633@pico-2018-shell:/problems/grep-1_4_0431431e36a950543a85426d0299343e$ cat file | grep "pico”
```
Finds the flag. `cat file` prints the contents of the file and by piping it into `grep ”pico”` filters out everything in the file that does not have `pico` in it and reveals the flag. 
##  Result
The flag is `picoCTF{grep_and_you_will_find_d66382d8}`

