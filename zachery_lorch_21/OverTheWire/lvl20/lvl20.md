# overthewire writeup
Zachery Lorch

### Leve21->level21
1. connect to server using ssh protcol

```bash
ssh bandit20@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. adhere to hit opening up another terminal and ssh into the same level21

4. then I open up a netcat listener and as well as providing the past password

```bash
echo {password} | nc -l -p 31000
```
breakdown:
the password will be echo to the port 31000 (randomly selected)

5. need to run the bin provided with my chosen port
```bash
./suconnect 31000
```
6. flag is sent to my port so my listener catches it 

flag: gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr