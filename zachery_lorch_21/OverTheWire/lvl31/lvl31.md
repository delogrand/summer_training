# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. go to my tmp file so I can edit things

```bash
cd /tmp/dummy
```

4. git clone with different name than repo and provide password

```bash
git clone ssh://bandit31-git@localhost/home/bandit27-git/repo bandit31
```
password is old key
5. if we cat the README.md we get easy instuctions to follow

```bash
cat README.md
```
file contents: 

This time your task is to push a file to the remote repository.

Details:
    File name: key.txt
    Content: 'May I come in?'
    Branch: master
	
6. well do what is says make a file called key and put the phase in it.

7. now add it to local with force

```bash
git add key.txt -f
```
8. commit

```bash
git commit -m"message"
```
9. push it 

```bash
git push
```

10. key is in validatioin message

flag: 56a9bf19c63d650ce78e6ec0354ee45e



