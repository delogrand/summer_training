# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 11 --> Level 12

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit11@bandit.labs.overthewire.org
```

2. Enter flag from bandit 10.11 **IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
** as 
password

3. Since the bandit site tells us the flag is stored in data.txt and is translated by 13 letters, we will translate command (tr) to shift the letters by 13. When shifted, A would route to N. Using this infomration, we know the last possible letter ro route to would be 13 before Z (or N). Therefore, we must then loop from M to Z. We repeat this for lowercase letters yeilding the following.
```bash
bandit11@bandit:~$ cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'
The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
```

The flag is: 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu

Helpful page:
https://en.wikipedia.org/wiki/ROT13