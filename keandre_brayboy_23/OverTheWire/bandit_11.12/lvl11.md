### Level 11 --> Level 12

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit11@bandit.labs.overthewire.org
```

2. Password can be found once lower and upper case letters are switched and rotated 13 positions.
```bash
$ cat data.txt | tr a-zA-Z n-za-mN-ZA-M
The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
```

Flag is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
