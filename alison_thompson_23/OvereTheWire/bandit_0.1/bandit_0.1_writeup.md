# Bandit Level 0 -> 1
## Problem
Find password for bandit1
## Solution
1. See what files are in the current directory with `ls`
1. There is a file called readme to view the contents use `cat readme` 

## Result
The password for bandit1 is in the readme file and is `oJ9jbbUNNfktd78OOpsqOltutMc3MY1` 
