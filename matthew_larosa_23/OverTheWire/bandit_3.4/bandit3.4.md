# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 3 --> Level 4

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit3@bandit.labs.overthewire.org
```

2. Enter flag from bandit 2.3 **UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK** as 
password

3. Read the files in the directory

```bash
bandit3@bandit:~$ ls
inhere
```

4. I noticed "inhere" is a directory so I cd in
```bash
bandit3@bandit:~$ cd inhere
bandit3@bandit:~/inhere$ ls
```

5. No files appear so we check for hidden files.
```bash
bandit3@bandit:~/inhere$ ls -a
.  ..  .hidden
```

6. Read the file with cat command
```bash
bandit3@bandit:~/inhere$ cat .hidden
pIwrPrtPN36QITSp3EQaw936yaFoFgAB
```

The flag is: pIwrPrtPN36QITSp3EQaw936yaFoFgAB
