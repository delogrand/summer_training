# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. go to my tmp file so I can edit things

```bash
cd /tmp/dummy
```

4. git clone and provide password

```bash
git clone ssh://bandit27-git@localhost/home/bandit27-git/repo
```
password is old key

5. cat the README that is in the repo 

```bash 
cat /repo/README
```
get flag

flag: 0ef186ac70e04ea33b4c1853d2526fa2