# PicoCTF Desrouleaux

 ## Problem 
Answer the questions given a file with information on various IT tickets. 
## Solution 
After connecting with `nc 2018shell.picoctf.com 40952` you are prompted with the first question
```
                                                                                                                           
                                                                                                                         What is the most common source IP address? If there is more than one IP address that is the most common, you may give any o
f the most common ones.                                                                                                    
```
Looking at the .json file, the most common IP address under `src_ip` is `13.198.222.191` which is the correct answer. 
                                                                                                                           
The next question is 
```                                                                                                                          
How many unique destination IP addresses were targeted by the source IP address 42.68.123.147?                             
```
Using `Control+F` and searching for `42.68.123.147` reveals two tickets with unique `dst_ip` and `42.68.123.147`as the `src_ip`. The correct answer is 2 and the next question is                                                                                                                            
                                                                                                                           
```
What is the number of unique destinations ips a file is sent, on average? Needs to be correct to 2 decimal places.          
```
Once again using `Control+F` in the `.json` file but this time searching for the unique file hashes and how many tickets they appear in. Out of the 8 tickets there are 4 files and two have been sent to three unique IP’s and two have been sent to one. Averaging this yields `(3+3+1+1)/4 = 2.00` giving the correct answer to be 2.00. After all questions are answered correctly the flag is received. 

                                                                                                                           
                                                                                                                         ##  Result
The flag is `picoCTF{J4y_s0n_d3rUUUULo_b6cacd6c}`

