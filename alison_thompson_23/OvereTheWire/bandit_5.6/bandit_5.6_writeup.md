# Bandit Level 5 -> 6 Write Up
 ## Problem: 
Find password for bandit6
## Solution 
1. See what files are in the current directory with `ls`, there is a directory called inhere
1. Switch to that directory using `cd inhere` 
1. Using `ls` shows the names of a ton of directories you have to search 
1. I used the command `file ./* –size 1033c` which searches all of the files in all of the directories of the current one for files that are 1033 bytes. In this case it was enough to narrow it down to one file `./maybehere07/.file2`
1. Use `cat ./maybehere07/.file2` to read the file

##  Result
The password for bandit6 is in the file and is `DXjZPULLxYr17uwoI01bNLQbtFemEgo7`

