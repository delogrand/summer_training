# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 10 --> Level 11

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit10@bandit.labs.overthewire.org
```

2. Enter flag from bandit 9.10 **truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
** as 
password

3. Since the bandit site tells us the flag is stored in data.txt and is encoded in base 64, we simpliy decode the data with base64 command and the d flag (or e flag).
```bash
bandit10@bandit:~$ base64 -d data.txt
The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
```

The flag is: IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR

Helpful page:
https://www.chiark.greenend.org.uk/~sgtatham/utils/base64.html