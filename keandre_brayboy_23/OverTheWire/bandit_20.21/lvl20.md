### Level 20 --> Level 21

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit20@bandit.labs.overthewire.org
```

2. Opened my own port listening for the correct password from the previous level.

```bash
$ echo 'GbKksEFF4yrVs6il55v6gwY5aVje5f0j' | nc -l localhost -p 1234
```

3. Opened a second terminal where I using the setuid binary.

```bash
$ ./suconnect 1234
Read: GbKksEFF4yrVs6il55v6gwY5aVje5f0j
Password matches, sending next password
```

4. Got the new password on the original terminal.

Flag is gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr