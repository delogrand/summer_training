﻿# Bandit Level 22-23 Write Up
 ## Problem: 
Find the password to level 23 using reoccurring commands ran utilizing cron. 
## Solution 
Switching to the directory `/etc/cron.d` shows ASCII text files labeled for different levels. Using `cat` to read cronjob_bandit23 shows that it runs a file in `/usr/bin/cronjob_bandit23.sh`. Using `cat` once again to read  this file shows: 
```
#!/bin/bash
“#!/bin/bash”
myname=$(whoami)
mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)
echo ““Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget”“
cat /etc/bandit_pass/$myname > /tmp/$mytarget

 ```
I assumed myname is bandit23 because that is who the user should be logged in as. Then, to find the variable mytarget I used the command ` echo I am user bandit23 | md5sum | cut -d ' ' -f 1` returning ` 8ca319486bfbbc3663ea0fbe81326349`. This then meant that the password was stored under `/tmp/ 8ca319486bfbbc3663ea0fbe81326349` so using `cat` found the password for level 23. 
```
bandit22@bandit:/etc/cron.d$ cat /tmp/8ca319486bfbbc3663ea0fbe81326349
jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n
```


##  Result
The password for bandit23 is ` jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n `

