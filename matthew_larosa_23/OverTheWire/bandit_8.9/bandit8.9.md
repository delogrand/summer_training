# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 8 --> Level 9

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit8@bandit.labs.overthewire.org
```

2. Enter flag from bandit 7.8 **cvX2JJa4CFALtqS87jk27qwqGhBM9plV** as 
password

3. Since the bandit site tells us the flag appears appears in data.txt and is the only lane that occurs once, we are able to link two commands. The sort command will sort the data and the uniq command will give us uniq adjacent results (with the -u flag).
```bash
bandit8@bandit:~$ sort data.txt|uniq -u
UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
```

The flag is: UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR

Helpful page:
https://www.geeksforgeeks.org/sort-command-linuxunix-examples/

https://www.geeksforgeeks.org/uniq-command-in-linux-with-examples/


https://unix.stackexchange.com/questions/11939how-to-get-only-the-unique-results-without-having-to-sort-data