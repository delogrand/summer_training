# overthewire writeup
Zachery Lorch

### Level3->level4
1. connect to server using ssh protcol

```bash
ssh bandit3@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. cd into inhere
```bash
cd inhere
```
4. use find command to see all files in the directory

```bash
find
```
5. cat the file named ./.hidden 
```bash 
cat ./.hidden
```

flag is: pIwrPrtPN36QITSp3EQaw936yaFoFgAB