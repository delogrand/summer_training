### OverTheWire Bandit0 Writeup

1. Connect to the server using SSH on port 2220
	$ ssh -p 2220 bandit0@bandit.labs.overthewire.org

2. Read files in the directory
	$ ls

3. Read the readme file
	$ cat readme

Flag is: boJ9jbbUNNfktd78OOpsqOltutMc3MY1