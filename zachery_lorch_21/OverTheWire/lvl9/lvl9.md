# overthewire writeup
Zachery Lorch

### Level9->level10
1. connect to server using ssh protcol

```bash
ssh bandit9@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. decode base64 data

```bash
base64 -d data.txt
```

flag is: IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
