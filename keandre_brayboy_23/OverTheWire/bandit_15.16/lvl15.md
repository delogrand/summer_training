### Level 15 --> Level 16

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit15@bandit.labs.overthewire.org
```
2. Connect to the local host on port 300001 using SSL encryption.
```bash
'BfMYroe26WYalil77FoDi9qh59eK5xNr' | openssl s_client -connect localhost:30001 -ign_eof
```
Outputed that I was correct and gave the password  cluFn7wTiGryunymYOu4RcffSxQluehd

Flag is cluFn7wTiGryunymYOu4RcffSxQluehd