### Recovering From the Snap

1. So for this a used a program called binwalker because I was completely lost on how to do this and some online help really helped.
2. With using the following command on the animals.dd file creates an extracter directory of all the jpeg images that we need.
```bash
$ binwalk --dd=".*":.jpg animals.dd
```
3. One of the images gives the flag picoCTF{th3_5n4p_happ3n3d}

Flag is picoCTF{th3_5n4p_happ3n3d}
