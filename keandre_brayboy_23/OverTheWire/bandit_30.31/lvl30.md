### Level 30 --> Level 31

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit30@bandit.labs.overthewire.org
```

2. Clone the git repository

```bash
$ git clone ssh://bandit30-git@localhost/home/bandit30-git/repo
```

3. Same as before lets check that readme file

```bash
$ cat /repo/README.md
just an epmty file... muahaha
```

4. There are no other branches time to try something different.

```bash
$ git branch -r
origin/HEAD -> origin/master
origin/master
```

5. Had to look this up because I was lost apparently a tag can log points in history

```bash 
$ git tag
secret
```

6.  Now to reveal the secret

```bash 
$ git show secret
47e603bb428404d265f59c42920d81e5
```

Flag is 47e603bb428404d265f59c42920d81e5