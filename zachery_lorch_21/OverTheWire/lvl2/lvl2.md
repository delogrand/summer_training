# overthewire writeup
Zachery Lorch

### Level2->level3
1. connect to server using ssh protcol

```bash
ssh bandit1@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. cat file with spaces
need to put name in '' because spaces make it seem like 
different commands
```bash
cat 'spaces in this filename'
```

flag is: UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK