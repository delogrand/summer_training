### Level 30 --> Level 31

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit31@bandit.labs.overthewire.org
```

2. Clone the git repository

```bash
$ git clone ssh://bandit31-git@localhost/home/bandit31-git/repo
```

3. Same as before lets check that readme file

```bash
$ cat /repo/README.md
This time your task is to push a file to the remote repository.

Details:
    File name: key.txt
    Content: 'May I come in?'
    Branch: master
```

4. Lets make that .txt file

```bash
$ echo "May I come in?" > key.txt 
```

5. Must remove the .gitignore file or it won't let me push my .txt file

```bash 
$ rm .gitignore
$ git add key.txt
```

6.  Alright time to commit and push.

```bash 
$ git commit -m "Upload a file"
$ git push
## Once it receives the proper file it outputs the line 
Well done! Here is the password for the next level:
remote: 56a9bf19c63d650ce78e6ec0354ee45e
```

Flag is 56a9bf19c63d650ce78e6ec0354ee45e