# Bandit Level 17-18 Write Up
 ## Problem: 
The password to level 18 is the only line that has been changed between two files called passwords.old and passwords.new
## Solution 
I used the command `diff –y passwords.new passwords.old` so it would show the files side by side and flag the line that is different. The output looked like: 
```
bandit17@bandit:~$ diff -y passwords.new passwords.old
sydIUj42mUfYK9xw1S9aPPB72rgagnxh                                sydIUj42mUfYK9xw1S9aPPB72rgagnxh
pcpkwztEjxg5EK0HABjmEvGUSCSdQW4F                                pcpkwztEjxg5EK0HABjmEvGUSCSdQW4F
LlomcOUT6d7lA2cJrYhCEhCChKCPrRao                                LlomcOUT6d7lA2cJrYhCEhCChKCPrRao
kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd                              | w0Yfolrc5bwjS4qw5mq1nnQi6mF03bii
TVzFbgWpqUPE4fwAJPCz4rT7GemAZUjz                                TVzFbgWpqUPE4fwAJPCz4rT7GemAZUjz
WCETP1i90TZJSbKZ24ly5rhNKva8sSdy                                WCETP1i90TZJSbKZ24ly5rhNKva8sSdy
2o4oJXwgWyWIdKb9WpNDFUcWXlcghSzR                                2o4oJXwgWyWIdKb9WpNDFUcWXlcghSzR
```
(with a much longer list). The line with the `|` character is the one that has been changed and the correct password corresponds with passowrds .new so it is the one on the left or ` kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd`
##  Result
The password for bandit18 is ` kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd`

