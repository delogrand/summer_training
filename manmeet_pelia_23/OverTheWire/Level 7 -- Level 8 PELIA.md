###### Shabri Pelia

### Level 7 --> Level 8

1. ssh bandit7@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed up
3. file data.txt - UTF-8 Unicode Text
4. grep millionth data.txt
5. Outputs the line with the string/word and the password next to it:
   - millionth	cvX2JJa4CFALtqS87jk27qwqGhBM9plV



TIPS/TAKE AWAYS:

* **grep** command: **grep string filename** - helps find a word in the file
* **piping** - you can do something and put  **|** and the output from the left side will do the command on the right side



