# overthewire writeup
Zachery Lorch

### Leve21->level22
1. connect to server using ssh protcol

```bash
ssh bandit21@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. go to the folder they said 

```bash 
cat /etc/cron.d
```
4. notice there is a cron task call cronjob_bandit22, if you try to run the cronjob_bandit22.sh 
you get premission denyed yet they show you that is is dumbing passwords in a file called
/tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv

5. cat this file and get flag


flag: Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI