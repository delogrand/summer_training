﻿# Bandit Level 13 -> 14 Write Up
 ## Problem: 
Find password for bandit14, the password is in a file that can only be accessed by the user bandit14 but you are given a private ssh key to sign in as bandit14
## Solution 
To ssh to bandit14 I used the command `ssh –i sshprivatekey –l bandit14 localhost`. The –i allows for the given private key to be used to establish the connection. Once there, I used the command `cat /etc/bandit_pass/bandit14` to find the password for bandit 14. 
##  Result
The password for bandit14 is ` 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e `

