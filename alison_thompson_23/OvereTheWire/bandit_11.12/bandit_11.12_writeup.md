# Bandit Level 11 -> 12 Write Up
 ## Problem: 
Find password for bandit12, the password stored in `data.txt` containing ROT13 encoded data
## Solution 
Use the `tr` command to decode the data, first use `cat data.txt` and then pipe it to `tr ”[A-Za-z]” ”[N-ZA-Mn-za-m]”’’ A to N is 13 characters, so the first character string in brackts is the regular alphabet and the second is the conversion that will be made (ex. ”[A-Za-z]” “[O-ZA-No-za-n]” would shift 14 characters to the right instead. 
```
bandit11@bandit:~$ cat data.txt | tr "[A-Za-z]" "[N-ZA-Mn-za-m]"
The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu```
##  Result
The password for bandit12 is ` IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR `

