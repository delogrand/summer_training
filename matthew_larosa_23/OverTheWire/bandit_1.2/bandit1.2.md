# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 1 --> Level 2

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit1@bandit.labs.overthewire.org
```

2. Enter flag from bandit 0.1 **boJ9jbbUNNfktd78OOpsqOltutMc3MY1** as password

3. Read the files in the directory

```bash
bandit1@bandit:~$ ls
-
```

4. Attempt to read the - directory

```bash
bandit1@bandit:~$ cat -

```
which then allows you to stop working

5. Use a relative path to specifiy the - dir
```bash
bandit1@bandit:~$ cat ./-
CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
```

Flag is CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
