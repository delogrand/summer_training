# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. go to my tmp file so I can edit things

```bash
cd /tmp/dummy
```

4. git clone with different name than repo and provide password

```bash
git clone ssh://bandit27-git@localhost/home/bandit27-git/repo bandit26
```
password is old key

5. notice that the current commit does not have the info we want. check all commits out

```bash
git log
```

6. read comments and checkout which commit we think has the info we want
```bash 
git checkout 186a1038cc54d1358d42d468cdc8e3cc28a93fcb
```
7. open readme and get flag

```bash 
cat README.md
```

flag: bbc96594b4e001778eee9975372716b2