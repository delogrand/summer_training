### pipe

1. Connect with 2018shell.picoctf.com 37542 and pipe the information to find the flag
2.```bash
$ nc 2018shell.picoctf.com 37542 | grep picoCTF
picoCTF{almost_like_mario_a6975cdb}
```

Flag is picoCTF{almost_like_mario_a6975cdb}