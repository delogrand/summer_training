### Level 2 --> Level 3 
1. Connect to the server 
```bash $ ssh -p 2220 bandit1@bandit.labs.overthewire.org
   Password: CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9```

2. Check for files
``
$ ls
``

3. Read the file
```$ cat spaces\ in\ this\ filename or cat s [tab]
UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
``` 
Flag is UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK

 
