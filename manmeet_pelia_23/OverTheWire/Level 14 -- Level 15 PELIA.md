###### Shabri Pelia

### Level 13 --> Level 14

1. ssh bandit13@bandit.labs.overthewire.org -p 2220
2. ls and show sshkey.private which is a key to get into bandit14 database
3. ssh -i sshkey.private bandit14@localhost
4. This process already gets me into level 14
5. Also find the password of getting into level 14 with cat /etc/bandit_pass/bandit14
6. Password found: 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e



TIPS/TAKE AWAYS:

* ssh -i(identity file) 'file with RSA private key' 'user@hostname of machine you are on'
