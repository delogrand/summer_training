### Level 9 --> Level 10

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit9@bandit.labs.overthewire.org
```

2. Search for the password in in a text file, one of the only human-readable strings, and begins with several "=" characters.
```bash
$ strings data.txt | grep '^='
========== password
========== isa
=FQ?P\U
=       F[
=)$=
========== truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
```

Flag is truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk