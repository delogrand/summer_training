# Bandit Level 19-20 Write Up
 ## Problem: 
You must use the given setuid to read the password for bandit20
## Solution 
The setuid escalates a user’s privileges temporarily. In order to read the password file I executed the setuid using `./bandit20-do` and then read the password file so in total the command was ` ./bandit20-do cat /etc/bandit_pass/bandit20`. 

```
bandit19@bandit:~$ ./bandit20-do cat /etc/bandit_pass/bandit20
GbKksEFF4yrVs6il55v6gwY5aVje5f0j
```
##  Result
The password for bandit20 is ` GbKksEFF4yrVs6il55v6gwY5aVje5f0j `

