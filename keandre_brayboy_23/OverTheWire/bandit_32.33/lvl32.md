### Level 32 --> Level 33

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit32@bandit.labs.overthewire.org
```

2. Apparently this switch all commands into uppercase, really could not follow what exactly was happening with this. Seems to be possible to use the following stuff to switch over the the usual format.

```bash
>> $0
$ export $SHELL=/bin/bash
$ echo $SHELL
/bin/bash
$ $SHELL
```

3. Just find the password in the usual spot now that everything is working like usual.

```bash
$ cat /etc/bandit_pass/bandit33
c9c3199ddf4121b10cf581a98d51caee
```

Flag is c9c3199ddf4121b10cf581a98d51caee