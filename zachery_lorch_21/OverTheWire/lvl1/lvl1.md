# overthewire writeup
Zachery Lorch

### Level1->level2
1. connect to server using ssh protcol

```bash
ssh bandit1@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *previous flag*

3. read '-'
cannot cat - file, it thinks it is stdin rather than actual
file, so cat needs to see that it is not just '-' so 
relavtive path to it works
```bash
cat .\-
```
flag is: CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9

