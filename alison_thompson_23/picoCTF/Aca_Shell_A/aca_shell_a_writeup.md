# PicoCTF Aca-Shell-A
 ## Problem 
You are connected to a shell but are restricted and want to gain permanent root access in order to find the flag. 
## Solution 
When first connecting it is revelaed that you have a helper that cannot see what you are typing only the output. Your goal is to find the username and password for the root. Typing `ls` reveals multiple directories but the only one that contains any files is `secret`. Once in the directory you are tasked with deleting all the intel files. I did this with the command `rm intel_*` where the asterisk makes it so all intel files are deleted in one command. The helper then gives an executable to help find the password, which is executed with the command `./dontLookHere`. After executing you are told that the helper cannot figure out the username and needs your help but you cannot use the echo command. To do this the command `whoami` prints the current username. Then, you are given a file in the /tmp directory to copy to the passwords dictory. This is accomplished with the command `cp /tmp/TopSecret passwords` from the main directly. Reading this file with `cat /passwords/TopSecret` reveals the flag along with Major General Schofield’s quote. 
##  Result
The flag is ` picoCTF{CrUsHeD_It_9edaa84a}`

