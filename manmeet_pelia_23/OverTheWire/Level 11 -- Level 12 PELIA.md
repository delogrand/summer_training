###### Shabri Pelia

### Level 11 --> Level 12

1. ssh bandit11@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed
3. file data.txt and ASCII text
4. cat data.txt and a segmented string of unreadable stuff is available - told that it is encoded using cypher rot13
5. **tr** command: translates character
6. On StackOverFlow: tr 'A-Za-z' 'N-ZA-Mn-za-m' is suppose to decode rot13 cypher
   - I don't understand what this really means, but according to the website A is replaced with N, lowercase Z replaced with m (but I need to ask someone about this)
7. cat data.txt | tr 'A-Za-z' 'N-ZA-Mn-za-m'
   - "The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu"



TIPS/TAKE AWAYS:

* **tr** command: translate and then put in a key afterwards
* **ROT13 cypher**: 'A-Za-z' 'N-ZA-Mn-za-m'



