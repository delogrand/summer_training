This problem involves decoding a password that was encoded in base64.

1. After accessing the level, find what files are in it
	'ls'
2. See a file called 'data.txt' that we know is encoded in base64. Decode that.
	'base64 -d data.txt'
3. This returns: 'The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR'
The flag is: IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR