# PicoCTF Reversing Warmup 2 
 ## Problem: 
Need to decode a string from base64 to ASCII format
## Solution 
Use the command `echo ”dGg0dF93NHNfczFtcEwz” | base64 -d` to return the string. This pipes the string into the command `base64 -d` which then decodes it and outputs the result. 
##  Result
The flag is `picoCTF{th4t_w4s_s1mpL3}`

