# Bandit Level 7 -> 8 Write Up
 ## Problem: 
Find password for bandit8
## Solution 
1. Use `ls` to view the files in the current directory (data.txt is only file)
1.  The password is in `data.txt` next to the word millionth, so to search use `cat data.txt | grep “millionth” ` where the pipe will use grep to search through the output of the first command, in this case all of the contents of the file  “data.txt”. Since it is searching for the word “millionth” it will print out the lines containing the word, in this case also printing the line with the password.  

##  Result
The password for bandit8 is `cvX2JJa4CFALtqS87jk27qwqGhBM9plV`

