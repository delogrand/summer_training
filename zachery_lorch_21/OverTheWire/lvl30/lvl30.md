# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* 

3. go to my tmp file so I can edit things

```bash
cd /tmp/dummy
```

4. git clone with different name than repo and provide password

```bash
git clone ssh://bandit30-git@localhost/home/bandit27-git/repo bandit30
```
password is old key

5. so there seems to be nothing here yet as after randomly try commands 
we find something 

```bash
git tag
```
and 

```bash
git ls-remote 
```
6. there is a tag called secert yet you cannot checkout this tag so what is it?

```bash
git cat-file f17132340e8ee6c159e0a4a6bc6f80e1da3b1aea
```
the string is the git hash of the object

7. it is a blob, never seen that before but we can read it and get a flag
```bash
git cat-file -p f17132340e8ee6c159e0a4a6bc6f80e1da3b1aea
```

flag: 47e603bb428404d265f59c42920d81e5