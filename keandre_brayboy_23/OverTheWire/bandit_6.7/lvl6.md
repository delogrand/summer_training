### Level 6 --> Level 7 

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit6@bandit.labs.overthewire.org
```

2. Search for files on the server that are owned by user bandit7 in the group bandit6 and has a file size of 33 bytes
```bash
$ find / -user bandit7 -group bandit6 -size 33c -type f
Gave a lot of files only one I had permission to access.
/var/lib/dpkg/info/bandit7.password
```

3. Read the file.
```bash
cat /var/lib/dpkg/info/bandit7.password
HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs
```

Flag is HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs



