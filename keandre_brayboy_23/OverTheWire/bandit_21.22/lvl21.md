### Level 20 --> Level 21

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit21@bandit.labs.overthewire.org
```

2. Password can be found in a program running automatically in /etc/cron.d/

```bash
$ ls /etc/cron.d/
atop  cronjob_bandit22  cronjob_bandit23  cronjob_bandit24
```

3. Read the cronjob_bandit22 file

```bash
$ cat cronjob_bandit22
@reboot bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
* * * * * bandit22 /usr/bin/cronjob_bandit22.sh &> /dev/null
```

4. Check the path on that file

```bash
$ cat /usr/bin/cronjob_bandit22.sh
#!/bin/bash
chmod 644 /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
cat /etc/bandit_pass/bandit22 > /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv

```

3. Read temp file created for the password of the next level

```bash
$ cat /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI
```
Flag is Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI