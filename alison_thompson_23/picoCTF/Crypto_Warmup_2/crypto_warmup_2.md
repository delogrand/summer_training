# PicoCTF Crypto Warmup 2
 ## Problem 
Need to decode `cvpbPGS{guvf_vf_pelcgb!}` given that it is encoded with rot13.
## Solution 
Rot13 is a cesar cipher with a shift of 13, so using either the `tr` command in linux or a cesar cipher decoder online gives the flag `picoCTF{this_is_crypto!}`
##  Result
The flag is `picoCTF{this_is_crypto!}`

