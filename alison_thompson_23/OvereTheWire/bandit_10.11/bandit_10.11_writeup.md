# Bandit Level 10 -> 11 Write Up
 ## Problem: 
Find password for bandit11, the password stored in `data.txt` containing base64 encoded data
## Solution 
The command `base64 –d filename` decodes base64 data so it can be used here.
```
bandit10@bandit:~$ base64 -d data.txt
The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
```
##  Result
The password for bandit11 is ` IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR `

