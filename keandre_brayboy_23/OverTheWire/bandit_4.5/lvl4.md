### Level 4 --> Level 5 

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit4@bandit.labs.overthewire.org
```

2. Search for files within a folder
```bash
$ ls
inhere
```

3. Seach for files inside of inhere
```bash
$ ls
file00
file01
file02
file03
file04
file05
file06
file07
file08
file09
```

4. Check which files are readable.
```bash
$ file ./file*
./-file00: data
./-file01: data
./-file02: data
./-file03: data
./-file04: data
./-file05: data
./-file06: data
./-file07: ASCII text
./-file08: data
./-file09: data
```

5. Read the file that has text that.
```bash
cat "./-file07"
koReBOKuIDDepwhWk7jZC0RTdopnAYKh
```

Flag is koReBOKuIDDepwhWk7jZC0RTdopnAYKh

