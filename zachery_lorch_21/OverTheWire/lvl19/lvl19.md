# overthewire writeup
Zachery Lorch

### Level9->level20
1. connect to server using ssh protcol

```bash
ssh bandit19@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. use the binary to cat the password in 20 

```bash
./bandit20-do cat /etc/bandit_pass/bandit20 
```

flag is: GbKksEFF4yrVs6il55v6gwY5aVje5f0j
