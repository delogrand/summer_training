# Bandit Level 8 -> 9 Write Up
 ## Problem: 
Find password for bandit9, the password is the only unique line in a file `data.txt`
## Solution 
1. The `uniq -u` command can only be used after the file is sorted, so use `sort data.txt | uniq –u` to find the unique line in the file 

##  Result
The password for bandit9 is `UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR `

