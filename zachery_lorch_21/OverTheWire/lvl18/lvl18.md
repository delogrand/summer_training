# overthewire writeup
Zachery Lorch

### Level7->level8
1. use scp command because ssh will never let you in 
and you know the file location and name 

```bash
scp -P 2220 bandit18@bandit.labs.overthewire.org:readme /home/username/Documents
```

2. cat readme and get flag

```bash
cat readme
```

flag is: IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x
