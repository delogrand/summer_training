###### Shabri Pelia

### Level 6 --> Level 7

1. ssh bandit6@bandit.labs.overthewire.org -p 2220
2. ls and nothing showed up
3. find / -user bandit7 -group bandit6 -size 33c
4. A bunch of files had permission denied but one didn't: **bandit7.password** 
5. 2>/dev/null command
   - 2 - takes all the standard errors
   - 1 - takes all the standard outputs
   - '>' redirects to whatever comes after
   - /dev/null - null device; takes anything redirected to it and throws it away (suppress outputs you don't want)
6. Outputs one file and when you cat that file 
   - Gives us the password: HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs



TIPS/TAKE AWAYS:

* '**find /**' will let you extend out of the root directory so you can look everywhere
* **2>/dev/null** command: gets rid of all the error stuff and only shows outputs that don't produce an error



