﻿# Bandit Level 12 -> 13 Write Up
 ## Problem: 
Find password for bandit13, the password stored in data.txt  which is a hexdump of a repeadetly compressed file
## Solution 
I first used the command `mkdir /tmp/test123` to create a directory under tmp and then copied the given file to that directory using the command `cp data.txt /tmp/test123`. After switching to that directory I undid the hexdump and sent it to a new file name using `xxd –r data2`. This command reverses the hexdump. After every step I used the `file *file name*` to check the type of file and the `mv` command to rename the file to the required extention (.gz, .bz). When the file was a tar directory, I used the command `tar –xvf` to extract the file from the archive and combinded with the commands `gunzip` and `bunzip2` was used until the remaining file was ASCII text and using the command `cat data8` the password was 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL.

##  Result
The password for bandit13 is  8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL" 
