# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit25@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. so need to look at all shells on machine

```bash
cat /etc/shells
```

4. we see that /usr/bin/showtext is a shell running and we cat it

```bash
cat /usr/bin/showtext
```

5. we notice that is uses the more command we also need to move the private key to our machine 

```bash
scp -P 2220 bandit25@bandit.labs.overthewire.org:bandit26ssh.key /home/username/Documents
```

6. so we log in with this private key yet we need to activate the more funciton so we make 
our terminal window very small. then we can hit v opening vim and in vim we can edit the file with :e
```bash
ssh -i bandit26ssh.key bandit26@bandit.labs.overthewire.org -p 2220
```

hit v 

type ```:e /etc/bandit_pass/bandit26```

flag should be shown

flag: 5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z