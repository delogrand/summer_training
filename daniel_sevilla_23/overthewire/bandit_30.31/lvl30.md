### Bandit Write Up
### Level 30 --> 31
ssh bandit30@bandit.labs.overthewire.org -p 2220
mkdir /tmp/gitb30
cd /tmp/gitb30
git clone bandit30-git@localhost:/home/bandit28-git/repo
cd repo
ls -la
cat README.md #Nothing in here...
git log #Must be in the gitpack?
cd ./.git/objects/pack
git verify-pack -v pack-de1...5fb.pack #Only one commit. Blobs? Has unused data. How to view?
git show 029ba421ef4c34205d52133f8da3d69bc1853777 #Showed the first file. Hmm...
git show f17132340e8ee6c159e0a4a6bc6f80e1da3b1aea #Has a flag
# Flag is 47e603bb428404d265f59c42920d81e5