### Level 1 --> Level 2 
1. Connect to the server 
```bash $ ssh -p 2220 bandit1@bandit.labs.overthewire.org```

2. Check for files
```bash
$ ls
```

3. Read the file by placing a path ahead of the name.
```bash
$ cat ./-
CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
```
Flag is CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
