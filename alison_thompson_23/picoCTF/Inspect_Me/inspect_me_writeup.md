# PicoCTF Inspect Me
 ## Problem 
Find the flag by ”inspecting” a given website.
## Solution 
Right clicking anywhere on the site and clicking ”inspect”. The main menu it brings up shows the html for the site and shows the comment `<!-- I learned HTML! Heres part 1/3 of the flag: picoCTF{ur_4_real_1nspe -->` giving the first part of the flag. Under sources there is a css file and in the code for it there is a comment ` I learned CSS! Heres part 2/3 of the flag: ct0r_g4dget_e96dd105} ` giving the second part of the flag. Finally under the javascript file in the sources tab has a third comment that says ` I learned JavaScript! Heres part 3/3 of the flag:  `. All together the flag is `picoCTF{ur_4_real_1nspect0r_g4dget_e96dd105}`
##  Result
The flag is `picoCTF{ur_4_real_1nspect0r_g4dget_e96dd105}`

