###### Shabri Pelia

### Level 8 --> Level9

1. ssh bandit8@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed up
3. file data.txt - ASCII text
   - This file just has a list of passwords and we have to go through them and just show the one that is unique
4. cat data.txt | sort | uniq -u
   - Gave one password: UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR

5. When testing count (cat data.txt | sort | uniq -c), every other line was repeated 10 times except the real password



TIPS/TAKE AWAYS:

* **sort** command: organizes the txt file as independent lines so each line becomes an input
  * -b: ignore leading blanks
  * -d: consider blanks and alphanumeric
  * -g: organize by numerical value
  * -r: reverse
* **uniq** command: filters through repeats
  * -u: only print unique lines
  * -c: counts how many repeats and put the number in front of the line



