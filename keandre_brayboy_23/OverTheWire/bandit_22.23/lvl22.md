### Level 22 --> Level 23

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit22@bandit.labs.overthewire.org
```

2. Check for the cron program running in /etc/cron.d/

```bash
$ls /etc/cron.d/
cronjob_bandit15_root  cronjob_bandit22  
cronjob_bandit24
cronjob_bandit17_root  cronjob_bandit23  cronjob_bandit25_root
```

3. Read the program running on bandit23

```bash
$ cat cronjob_bandit23
@reboot bandit23 /usr/bin/cronjob_bandit23.sh  &> /dev/null
* * * * * bandit23 /usr/bin/cronjob_bandit23.sh  &> /dev/null
bandit22@bandit:/etc/cron.d$ cat /usr/bin/cronjob_bandit23.sh
#!/bin/bash

myname=$(whoami)
mytarget=$(echo I am user $myname | md5sum | cut -d ' ' -f 1)

echo "Copying passwordfile /etc/bandit_pass/$myname to /tmp/$mytarget"

```

4. Set name to bandit23

```bash
$ myname=bandit23

```

3. Run the echo command from the cron program

```bash
$ echo I am user $myname | md5sum | cut -d ' ' -f 1
8ca319486bfbbc3663ea0fbe81326349

```

4. Read the file.

```bash
cat /tmp/8ca319486bfbbc3663ea0fbe81326349
jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n

```
Flag is jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n