### Level 19 --> Level 20

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit19@bandit.labs.overthewire.org
```

2. Must use setuid binary in the homedirectory.

```bash
$ ./bandit20-do
Run a command as another user
```

3. Using this info check for the password in the usual spot.
```bash
$ ./bandit20-do cat /etc/bandit_pass/bandit20
GbKksEFF4yrVs6il55v6gwY5aVje5f0j
```

Flag is GbKksEFF4yrVs6il55v6gwY5aVje5f0j