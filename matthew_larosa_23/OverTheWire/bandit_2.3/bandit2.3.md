# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 2 --> Level 3

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit2@bandit.labs.overthewire.org
```

2. Enter flag from bandit 1.2 **CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9** as password

3. Read the files in the directory

```bash
bandit2@bandit:~$ ls
spaces in this filename
```

4. Read the file with quotations to prevent parsing of the string

```bash
bandit2@bandit:~$ cat "spaces in this filename"
UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
```

Flag is CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
