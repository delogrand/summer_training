# overthewire writeup
Zachery Lorch

### Level1->level2
1. connect to server using ssh protcol

```bash
ssh bandit11@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. so using tr to translate one set to another set, all A-Z needs to be N-ZA-M
this is moving it over by 13 spaces, smae for lower case

```bash
cat data.txt| tr 'A-Za-z' 'N-ZA-Mn-za-m'
```

flag is: 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
