### Level 16 --> Level 17

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit16@bandit.labs.overthewire.org
```

2. Find active ports on the localhost in the range of 31000 to 32000
```bash
$ nmap localhost -p 31000-32000
```

3. Tested each of the ports returned until given a private key using a variation of the command below
```bash
echo 'cluFn7wTiGryunymYOu4RcffSxQluehd' | openssl s_client -connect localhost:#####  -ign_eof
```
4. Used the private key to connect to next level.
5. Password for level 17 can now be found.
```bash
$ cat /etc/bandit_pass/bandit17
xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn
```
Flag is xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn

