# Bandit Level 14-15 Write Up
 ## Problem: 
Find password for bandit15 by submitting the password to bandit14 on port 3000 on local host
## Solution 
I used the command `nc localhost 30000` first connect to port 3000 on the local host.  `nc` or Netcat is a tool used to read and write data over networks. Once the connection was established, I typed the password for bandit 13 and received the response: 
```
bandit14@bandit:/etc/bandit_pass$ nc localhost 30000
4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e
Correct!
BfMYroe26WYalil77FoDi9qh59eK5xNr

```
##  Result
The password for bandit15 is ` BfMYroe26WYalil77FoDi9qh59eK5xNr `

