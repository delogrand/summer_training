# overthewire writeup
Zachery Lorch

### Leve25->level26
1. connect to server using ssh protcol

```bash
ssh bandit26@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag* yet make the terminal really small to 
activate the more script. Then hit v to enter vim

3. in vim you can actaully set shell so lets do that 

```bash
:set shell=/bin/bash
```

4. know enter the shell

```bash
:shell
```
5. now we are just on a normal shell line. we see another binary similar to other challenges 
so let us do the same thing 

```bash
./bandit27-do cat /etc/bandit_pass/bandit27-do
```
flag is shown

flag: 3ba3118a22e93127a4ed485be72ef5ea