# PicoCTF strings 
 ## Problem 
Find the flag in a file with non human readable characters.  
## Solution 
The `strings` command will print the human readable characters of a file. This combined with a pipe to grep finds the flag using the full command `strings strings | grep ”pico” `. 
##  Result
The flag is ` picoCTF{sTrIngS_sAVeS_Time_3f712a28}`

