### Level 26 --> Level 27

1. Connect to the server through the use of bandit 25

2. Change it so we can execute stuff in a shell

```bash
:set shell=/bin/bash
:shell
```

3.  Lets see what's around

```bash
$ ls
bandit27-do  text.txt
```

4.  Lets see what bandit27-do does

```bash
$ ./bandit27-do
Run a command as another user.
  Example: ./bandit27-do id
```

5. Okay lets grab the password.

```bash
$ ./bandit27-do cat /etc/bandit_pass/bandit27
3ba3118a22e93127a4ed485be72ef5ea
```

Flag is 3ba3118a22e93127a4ed485be72ef5ea