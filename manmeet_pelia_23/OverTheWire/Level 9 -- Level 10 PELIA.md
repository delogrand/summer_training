###### Shabri Pelia

### Level 9 --> Level 10

1. ssh bandit9@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed
3. file data.txt and it's just data
   - a bunch of wack stuff shows up if you trying to cat the file
4. If you do **strings data.txt**, it will show all the human readable strings 
5. **strings data.txt | grep '===' ** , showed me all the strings with === and there were only four, one of them had the password
   - Password is truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk



TIPS/TAKE AWAYS:

* **strings** command: "strings filename" - will just show all the parts of a data file that are human readable strings



