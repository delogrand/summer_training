### Crypto_Warmup_1

1. rot13 the message cvpbPGS{guvf_vf_pelcgb!}
2. Follow the same method from bandit 11.
3. 
```bash
$ echo 'cvpbPGS{guvf_vf_pelcgb!}' | tr a-zA-Z n-za-mN-ZA-M
picoCTF{this_is_crypto!}
```

Flag is picoCTF{this_is_crypto!}