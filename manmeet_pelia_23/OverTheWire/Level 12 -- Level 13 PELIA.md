###### Shabri Pelia

### Level 12 --> Level 13

1. ssh bandit12@bandit.labs.overthewire.org -p 2220
2. ls and data.txt showed
3. file data.txt and ASCII text
4. Have to make a new directory and put a copy of data.txt in it so it doesn't get messed up when decoding
5. **mkdir/tmp/shabri123** (tmp means temporary)
6. **cp data.txt /tmp/shabri123** : made a copy of data.txt and put it in the new temporary folder
7. **mv data.txt copyofdata.txt** : renamed the data.txt as copy so I can make the differentiation
8. **xxd -r copyofdata.txt > bandit** : xxd can make a hex dump or reverses it (-r) and > takes the output of the xxd and assigns it to a new file called 'bandit'
9. **file bandit** : gzip compressed data
10. **zcat bandit > bandit1** : decompresses bandit and takes output to new file, bandit1
11. **bzip2 -d bandit1** : decompresses bandit1 and creates bandit1.out
12. **file bandit1.out** : gzip compressed data
13. **zcat bandit1.out > bandit2**
14. **file bandit2**: POSIX tar archive
15. **tar -xvf bandit2** : data5.bin
16. Keep doing this over and over by reading the file and using the proper command until you unzip data8.bin and read the file
    - "The password is 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL"



TIPS/TAKE AWAYS:

* **Hex Dump**: hexadecimal view of data
* **cp** command: cp filename /folder - copies file to a certain folder
* **mv** command: mv ogfile newfilename - change name of a file 
* **xxd** command: makes a hex dump or reverses it with (-r)
* **zcat** command: decompresses a file without overwriting the original file so needs a '> filename' if wanting to assign the output to a different file
* **gzip** command: compresses the file into a gzip compressed data
* **gunzip** command: decompresses a gzip file and overwrites the file (does not over the file if using **-c**)
* **bzip2** command: compresses file to bzip2, but decompresses if using (**-d**)
* **tar -xvf** command: decompresses the POSIX tar archive file type



