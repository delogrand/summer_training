## Cyber Team Summer Training

### Topics and training:

[Resources](./Resources.md)



### Compete on your own:

Feel free to sign up for some online CTFs and compete in them. There are many that are open year round, but for timed CTFs and even ones with prizes: https://ctftime.org/event/list/upcoming



Note: You do not have to complete any of the competitions and levels in order.  Also, we are not requiring you to finish every single level or even start every competition.  We want you as exposed to cyber as possible before joining the team.  These competitions are purely here for you to get to know the types of things the team works on and let you practice before we invite you to the team.



If you are stuck on a problem, go to another one. Or even a whole different competition. I’ve been stuck on a single problem for 3 weeks so I get it. We will be updating this document with different competitions as time goes on so if you get bored or stuck in a competition, check back for new things to work on.  Push through, google help, and work with others. We are always happy to help you get started or help you get on the right path.