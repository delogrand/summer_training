# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 9 --> Level 10

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit9@bandit.labs.overthewire.org
```

2. Enter flag from bandit 8.9 **UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
** as 
password

3. Since the bandit site tells us the flag is stored in data.txt and is readable with === before it, we run the following commmand
```bash
bandit9@bandit:~$ strings data.txt | grep ===
2========== the
========== password
========== isa
========== truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
```

The flag is: truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk

Helpful page:
https://www.howtoforge.com/linux-strings-command/