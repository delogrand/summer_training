### Level 17 --> Level 18

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit17@bandit.labs.overthewire.org
```

2. Find the password in the only line that has been changed between the two files.
```bash
$ diff passwords.old passwords.new
< hlbSBPAWJmL6WFDb06gpTx1pPButblOA
---
> kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd
```

Flag is kfBf3eYk5BPBRzwjqutbbfE887SVc5Yd
