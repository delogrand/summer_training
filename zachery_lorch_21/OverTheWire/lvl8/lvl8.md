# overthewire writeup
Zachery Lorch

### Level8->level9
1. connect to server using ssh protcol

```bash
ssh bandit8@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. sort data and send it to uniq 

```bash
sort data.txt | uniq -u
```

flag is: UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
