# Bandit Level 9 -> 10 Write Up
 ## Problem: 
Find password for bandit10, the password a human readable line beginning with several equal signs in the file `data.txt`
## Solution 
`cat data.txt` does not work in this case because the file is not completely readable. To print the human readable lines, I used `strings data.txt`, and then they needed to be searched for equal signs so I added a grep looking for those. 
```
bandit9@bandit:~$ strings data.txt | grep "======"
2========== the
========== password
========== isa
========== truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
```
##  Result
The password for bandit10 is ` truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk `
