# PicoCTF Grep 2
 ## Problem 
The flag is a line of text in one of many possible files across multiple directories. 
## Solution 
The command `grep -r` recursively searches the entire directory for the given string, so ` grep -r ”pico”` shows that the flag in the directory files5 and the file file24. 
```
alit633@pico-2018-shell:/problems/grep-2_2_413a577106278d0711d28a98f4f6ac28/files$ grep -r "pico"                          
files5/file24:picoCTF{grep_r_and_you_will_find_8eb84049}
```
##  Result
The flag is ` picoCTF{grep_r_and_you_will_find_8eb84049}`
