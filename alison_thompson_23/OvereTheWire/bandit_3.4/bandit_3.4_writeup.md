# Bandit Level 3 -> 4 Write Up
 ## Problem: 
Find password for bandit4
## Solution 
1. See what files are in the current directory with `ls`, there is a directory called inhere
1. switch to that directory using `cd inhere` 
1. Using `ls` shows nothing in the directory but using `ls –a` shows a hidden file called `.hidden`
1. use `cat .hidden` to view the contents 

##  Result
The password for bandit4 is in the file and is `pIwrPrtPN36QITSp3EQaw936yaFoFgAB`

