### Level 10 --> Level 11

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit10@bandit.labs.overthewire.org
```

2. The password stored in a data.txt file and is encoded is base64.
```bash
$ base64 -d data.txt
The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
```

Flag is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR