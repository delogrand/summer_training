# PicoCTF General Warmup 3 
 ## Problem: 
Need to convert the number 0x3D (base 16) to binary (base 2).
## Solution 
The 0 gives `16**2 * 0`. Then 3 gives `16**1 * 3` and D gives `16**0 * 13`. This gives`*48 + 13 = 61`.
##  Result
The flag is `picoCTF{61}`

