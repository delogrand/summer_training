### Desrouleaux

1. Well this one just took read the json file and answering the questions given.
2. What is the most common source IP address? If there is more than one IP address that is the most common, you may give any of the most common ones.
13.198.222.191
Correct!
3. How many unique destination IP addresses were targeted by the source IP address 13.198.222.191?
3
Correct!
4. What is the number of unique destination ips a file is sent, on average? Needs to be correct to 2 decimal places.
2.00
Correct!

Great job. You've earned the flag: picoCTF{J4y_s0n_d3rUUUULo_b6cacd6c}

Flag is picoCTF{J4y_s0n_d3rUUUULo_b6cacd6c}
