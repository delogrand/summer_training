### Inspect Me

1. Time for some inspect element fun for finding the flag.
2. The first portion can be found in a comment made under the abouttabs html
 I learned HTML! Here's part 1/3 of the flag: picoCTF{ur_4_real_1nspe 
3. The second portion can be found in a comment made under the mycss.css portion of data for the website.
/* I learned CSS! Here's part 2/3 of the flag: ct0r_g4dget_b4887011} */
4. THe final portion can be found in a comment made under the javascript myjs.js.
* I learned JavaScript! Here's part 3/3 of the flag:  */

Flag is picoCTF{ur_4_real_1nspect0r_g4dget_b4887011}
