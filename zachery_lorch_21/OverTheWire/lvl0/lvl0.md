# overthewire writeup
Zachery Lorch

### Level0->level1
1. connect to server using ssh protcol

```bash
ssh bandit0@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password **bandit0**

3. read readme.md

```bash
cat readme.md
```
flag is: boJ9jbbUNNfktd78OOpsqOltutMc3MY1
