### Level 7 --> Level 8

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit7@bandit.labs.overthewire.org
```

2. Search for the password in the data.txt file. The password is next to the word millionth.
```bash
$ grep 'millionth' data.txt
millionth       cvX2JJa4CFALtqS87jk27qwqGhBM9plV
```

Flag is cvX2JJa4CFALtqS87jk27qwqGhBM9plV