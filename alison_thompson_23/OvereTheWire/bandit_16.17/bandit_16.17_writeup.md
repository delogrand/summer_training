﻿# Bandit Level 16-17 Write Up
 ## Problem: 
Find password for bandit17 by submitting the password to bandit16 to the server in the port range 31000-32000 that understands SSL.

## Solution 
To determine which ports were up in the range 31000-32000 I used the `nmap` command to scan those ports. Using `nmap –p 31000-32000` returns 5 possible ports. 
```
bandit16@bandit:~$ nmap -p 31000-32000 localhost
Starting Nmap 7.40 ( https://nmap.org ) at 2020-06-02 20:52 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00024s latency).
Not shown: 996 closed ports
PORT      STATE SERVICE
31046/tcp open  unknown
31518/tcp open  unknown
31691/tcp open  unknown
31790/tcp open  unknown
31960/tcp open  unknown
```

From there I used the command in the previous level `openssl s_client –connect localhost:PORT#` for each of the found ports and only 31790 was able to establish a connection. Pasting the password for the current level returned an RSA private key. 
I then switched the directory to /tmp/ so I would be able to create a file with the private key. I used the echo command to send the key to a file I called `sshprivatekey.txt`. I then used the command `chmod 600 sshprivatekey.txt` so I would have full rwx privleges for the file. Last, I used the same ssh command from bandit 14 `ssh –i sshprivatekey.txt –l bandit17 localhost` and was able to connect to bandit17. Once in bandit17 I sued the command `cat /etc/bandit_pass/bandit17` to find that the password for bandit17 is `xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn`
##  Result
The password for bandit17 is ` xLYVMN9WE5zQ5vHacb0sZEVqbrp7nBTn

