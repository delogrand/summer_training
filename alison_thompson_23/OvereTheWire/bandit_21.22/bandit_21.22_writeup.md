# Bandit Level 21-22 Write Up
 ## Problem: 
Find the password to level 22 using reoccurring commands ran utilizing cron. 
## Solution 
Switching to the directory `/etc/cron.d` shows ASCII text files labeled for different levels. Using `cat` to read cronjob_bandit22 shows that it runs a file in `/usr/bin/cronjob_bandit22.sh`. Using `cat` once again to read  this file shows: 
```
#!/bin/bash
chmod 644 /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
cat /etc/bandit_pass/bandit22 > /tmp/t7O6lds9S0RqQh9aMcz6ShpAoZKF7fgv
```
This means it reads the password for level 22 and sends it to a file in `/tmp`. Reading this file with cat shows the password for level 22 which is Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI. 
##  Result
The password for bandit22 is ` Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI `

