﻿# Bandit Level 20-21 Write Up
 ## Problem: 
Find the password to level 21
## Solution 
This setuid searches for the password to the current level on a port you specify, if it finds it then it gives you the password to the next leve. To accomplish this I opened two command prompts and connected them both to bandit 20. On one I used the command `echo ”GbKksEFF4yrVs6il55v6gwY5aVje5f0j" | nc -l localhost -p 43999` this command listens on port 43999 while echoing the password for bandit20. I used port 43999 because it only allowed permission for the higher port numbers. In a sepertae terminal I then executed suconnect on the port 43999 with the command `./suconnect 43999`. It found that the password matched the current level and returned the password for bandit 21 on the terminal that was listening.
```
bandit20@bandit:~$ echo ”GbKksEFF4yrVs6il55v6gwY5aVje5f0j” | nc -l localhost -p 43999
gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr
```

##  Result
The password for bandit21 is ` gE269g2h3mw3pwgrj0Ha9Uoqen1c9DGr `

