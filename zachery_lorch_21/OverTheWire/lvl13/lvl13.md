# overthewire writeup
Zachery Lorch

### Level3->level4
1. connect to server using ssh protcol

```bash
ssh bandit13@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. move sshkey.private to local machine

```bash
scp -P 2220 bandit13@bandit.labs.overthewire.org:sshkey.private /home/username/Documents/
```

4. need to update permissions 
```bash
chmod 600 sshkey.private
```
`
5. log into server with bandit13 user name, no password 
should be neededd

```bash
ssh -i sshkey.private bandit13@bandit.labs.overthewire.org -p 2220
```
6. cat the file the password is in 

```bash
cat /etc/bandit_pass/bandit14
```

flag is: 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e

