### net_cat

1. Using netcat (nc) will be a necessity throughout your adventure. Can you connect to 2018shell.picoctf.com at port 36356 to get the flag?
2. Yeah so bash didn't have the netcat command so I used the virtual machine.
3.
```bash
$ nc 2018shell.picoctf.com 36356
picoCTF{NEtcat_iS_a_NEcESSiTy_9454f3e0}
```

Flag is picoCTF{NEtcat_iS_a_NEcESSiTy_9454f3e0}