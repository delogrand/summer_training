# overthewire writeup
Zachery Lorch

### Level2->level3
1. connect to server using ssh protcol

```bash
ssh bandit12@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. THIS SUCKED 
4. first reversed the hex dumb 

```bash
xxd -r data.txt > test
```

5. then check the compression type with 'file' command 

```bash
file test
```
6. once I know the file type I would rename the file to have 
the correct compression extension
for gzip .gz
for bzip2 .bz2
for tar .tar
```bash
mv test test.gz
```
7. then I would uncompress the file with the correct 
command 
so gzip -d test.gz or
bunzip2 test.bz  or
tar -xvf test.tar


```bash
tar -xvf test.tar
```
8. these steps were repeated until the file type was ASCII then it was 
opened and read

```bash
cat test
```

flag is: 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL

