### Level 12 --> Level 13

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit12@bandit.labs.overthewire.org
```

2. Reverse the hex dump file.
```bash
$ xxd -r data.txt > test
```

3. Check the file type.
```bash
$ file test
Originally a gzip file
```
4. Renamed the file to the appropriate type.
```bash
$ mv test data.gz
```
5. Check the file type
6. Basically kept decompressing a checking file types until something with ASCII text appeared.
7. Last file for me was data8
```bash
$ cat data8
The password is 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL
```

Flag is 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL

