# overthewire writeup
Zachery Lorch

### Level5->level6
1. connect to server using ssh protcol

```bash
ssh bandit5@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. cd into inhere
```bash
cd inhere
```
4. use find to look in all directories with given size andnot executable 
as well as human text

```bash
find . -type f -size 1033c ! -exec file {} + | grep text
```
breaking this down looking for a file of size 1033 that is not (!) 
an exec file and we pipe that with a grep for texxt
5. cat ./maybehere07/.file2  

```bash 
cat /maybehere07/.file2
```

flag is: DXjZPULLxYr17uwoI01bNLQbtFemEgo7