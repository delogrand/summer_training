### Bandit Write Up
### Level 28 --> 29
ssh bandit28@bandit.labs.overthewire.org -p 2220
mkdir /tmp/gitb28
cd /tmp/gitb28
git clone bandit28-git@localhost:/home/bandit28-git/repo
cd repo
ls -la
cat README.md #Looks like file might have had revisions
git log README.md #One commit had the password in it
git reset --hard c086d11 #Go back to commit with password
cat README.md
# Flag is bbc96594b4e001778eee9975372716b2