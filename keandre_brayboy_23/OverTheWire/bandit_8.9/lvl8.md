### Level 8 --> Level 9

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit8@bandit.labs.overthewire.org
```

2. Search for the password in in a text file, the only line that occurs once.
```bash
$ sort data.txt | uniq -u
UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
```

Flag is UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR