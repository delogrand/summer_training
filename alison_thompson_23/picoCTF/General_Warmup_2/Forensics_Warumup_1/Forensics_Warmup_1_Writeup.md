# PicoCTF Forensics Warmup 1 
 ## Problem: 
Given a zipped file and need to retrieve the flag 
## Solution 
Once the file is downloaded right click on the folder and select ”Extract All”, the file extracted is the flag. 
##  Result
The flag is `picoCTF{welcome_to_forensics}`

