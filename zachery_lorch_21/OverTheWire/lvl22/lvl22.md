# overthewire writeup
Zachery Lorch

### Leve22->level23
1. connect to server using ssh protcol

```bash
ssh bandit21@bandit.labs.overthewire.ord -p 2220
```
2. entering in the password *pervious flag*

3. go to the folder they said 

```bash 
cat /etc/cron.d
```
4. I open the .sh that is made and read what it is doing. 

```bash
cat /usr/bin/cronjob_bandit23.sh
```

5. from this you can see it is an md5um hash of the phrash I am user banditX 
X being the current user you are. Well we are only seeing bandit22 yet we want 
bandit23. We can assume that someone before us has had this .sh run as user bandit23
So we just find the hash when X=23 

```bash
echo I am user bandit23 | md5sum | cut -d ' ' -f 1
```

6. then we cat the file which is in the tmp folder and get the flag


flag: jc1udXuA1tiHqjIsL8yaapX5XIAI6i0n