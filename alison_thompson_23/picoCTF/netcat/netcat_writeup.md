# PicoCTF netcat 
 ## Problem 
Need to find the flag by connecting to a given host on a specific port using netcat. 
## Solution 
Using the command 
```
alit633@pico-2018-shell:/problems/grep-1_4_0431431e36a950543a85426d0299343e$ netcat 2018shell.picoctf.com 22847             
```
This command connects to the given host on a tcp connection using the port 22847 and returns the flag. 
##  Result
The flag is ` picoCTF{NEtcat_iS_a_NEcESSiTy_69222dcc} `

