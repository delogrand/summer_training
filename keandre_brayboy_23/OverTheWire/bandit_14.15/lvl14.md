### Level 14 --> Level 15

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit14@bandit.labs.overthewire.org
```

2. Now connect to the serve on port 30000 on localhost
```bash
$ nc localhost 30000 4wcYUJFw0k0XLShlDzztnTBHiqxU3b3e
Correct!
BfMYroe26WYalil77FoDi9qh59eK5xNr
```

Flag is BfMYroe26WYalil77FoDi9qh59eK5xNr