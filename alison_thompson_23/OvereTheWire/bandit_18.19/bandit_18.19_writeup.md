# Bandit Level 18-19 Write Up
 ## Problem: 
The password to level 19 is stored in a file in the home directory but every time you login with ssh it kicks you out. 
## Solution 
Since the connection is immediately terminated every time bandit19 logs on, I tried to execute the connection and reading of the file in one line using the command `ssh –l bandit18 localhost cat readme` (signing in from bandit17). This worked and the password found was `IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x`
##  Result
The password for bandit19 is ` IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x `

