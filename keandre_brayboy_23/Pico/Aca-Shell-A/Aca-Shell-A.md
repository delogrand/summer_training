### Aca-Shell-A

1. Well this was a fun ride. Connect to the server using nc 2018shell.picoctf.com 58422
2. Now go to the secret directory and remove the intel files.
```bash
$ rm in*
```
3. Following that do as your told to get the executable file and then run it.
```bash
$ ./dontLookHere
```
4. With that find out who we are.
```bash
$ whoami
l33th4x0r
```
5. With that quickly copy the file over to the passwords folder and read it.
```bash
$ cp /tmp/TopSecret passwords
$ cat TopSecret
Major General John M. Schofield's graduation address to the graduating class of 1879 at West Point is as follows: The discipline which makes the soldiers of a free country reliable in battle is not to be gained by harsh or tyrannical treatment.On the contrary, such treatment is far more likely to destroy than to make an army.It is possible to impart instruction and give commands in such a manner and such a tone of voice as to inspire in the soldier no feeling butan intense desire to obey, while the opposite manner and tone of voice cannot fail to excite strong resentment and a desire to disobey.The one mode or other of dealing with subordinates springs from a corresponding spirit in the breast of the commander.He who feels the respect which is due to others, cannot fail to inspire in them respect for himself, while he who feels,and hence manifests disrespect towards others, especially his subordinates, cannot fail to inspire hatred against himself.
picoCTF{CrUsHeD_It_4e355279}
```

Flag is picoCTF{CrUsHeD_It_4e355279}
