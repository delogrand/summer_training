### Level 18 --> Level 19

1. Connect to the server 

```bash 
$ ssh -p 2220 bandit18@bandit.labs.overthewire.org
```

2. When trying to connect, the server repeatedly closes before anything can be done.
3. Given information that the password is in a readme file in the home directory. So connect and read the file in the same command.

```bash
$ ssh -p 2220 bandit18@bandit.labs.overthewire.org cat './readme'
IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x
```

Flag is IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x