# OverTheWire Bandit Writeup

Matthew LaRosa

### Level 7 --> Level 8

1. Connect to the server via SSH on port 2220

```bash
$ ssh -p 2220 bandit7@bandit.labs.overthewire.org
```

2. Enter flag from bandit 6.7 **HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs** as 
password

3. Since the bandit site tells us the flag appears after the string millionth, we use the grep command.
```bash
bandit7@bandit:~$ grep millionth data.txt
millionth       cvX2JJa4CFALtqS87jk27qwqGhBM9plV
```

The flag is: cvX2JJa4CFALtqS87jk27qwqGhBM9plV

Helpful page:
https://docs.oracle.com/cd/E19455-01/806-2902/6jc3b36dn/index.html