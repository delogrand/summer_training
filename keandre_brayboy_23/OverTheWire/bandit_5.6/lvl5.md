### Level 5 --> Level 6 

1. Connect to the server 
```bash 
$ ssh -p 2220 bandit5@bandit.labs.overthewire.org
```

2. Search for files within the inhere folder that are human-readable, 1033 bytes, and are not executable.
```bash
$ find ./ -type f -size 1033c ! -executable
./maybehere07/.file2
```

3. Read the file.
```bash
cat ./maybehere07/.file2
DXjZPULLxYr17uwoI01bNLQbtFemEgo7
```

Flag is DXjZPULLxYr17uwoI01bNLQbtFemEgo7


