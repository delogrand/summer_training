### grep 2

1. Time to use grep again to find another flag located in the problem on the shell server at /problems/grep-2_0_783d3e2c8ea2ebd3799ca6a5d28fc742/files
2. Not gonna lie, took a second to take in the amount files I would have to search through, but then found out some more interesting things about the grep command.
3. 
```bash
$ cd /problems/grep-2_0_783d3e2c8ea2ebd3799ca6a5d28fc742/files
$ grep picoCTF -r *         
files5/file23:picoCTF{grep_r_and_you_will_find_24c911ab}
```

Flag is picoCTF{grep_r_and_you_will_find_24c911ab}
