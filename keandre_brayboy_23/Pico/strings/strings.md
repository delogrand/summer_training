### strings 

1. Find the flag without running the file.
2.```bash
$ strings strings | grep picoCTF
picoCTF{sTrIngS_sAVeS_Time_d7c8de6c}
```

Flag is picoCTF{sTrIngS_sAVeS_Time_d7c8de6c}