# Bandit Level 15-16 Write Up
 ## Problem: 
Find password for bandit16 by submitting the password to bandit 15 on port 30001 on local host *using SSL encryption*
## Solution 
To establish the connection to port 30001 on localhost using ssl I used the command `openssl s_client -connect localhost:30001`. Once connected I then typed the password for the previous level ` BfMYroe26WYalil77FoDi9qh59eK5xNr` and was given the password for bandit 16 in return. 

```
bandit14@bandit: openssl s_client -connect localhost:30001
…
BfMYroe26WYalil77FoDi9qh59eK5xNr
Correct!
cluFn7wTiGryunymYOu4RcffSxQluehd

closed
```
##  Result
The password for bandit16 is ` cluFn7wTiGryunymYOu4RcffSxQluehd`

